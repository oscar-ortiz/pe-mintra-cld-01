'use strict';
/* For production domain with https */
window.API_ROOT = "/_ah/api";
// Global for replace in tinymce
window.findDomain = function() {
    // Validate exists Domain

    if (!window.DOMAIN) {

        if( location.href.match(/\/a\/.+/i) ) {
            window.DOMAIN = location.href.match(/\/a\/.+/i);
            // split url multitenant
            window.DOMAIN = window.DOMAIN[0].substr(3).replace(/\/.+/ig,'');
            console.info('window.DOMAIN was extract from Url tenant: ', window.DOMAIN);

        } else if ( window.user_email ) {
            window.DOMAIN = window.user_email.match(/@.+$/)[0].replace('@', '');
            console.warn('window.DOMAIN was extract from to split g.user_email:', window.DOMAIN );
        }
    }
    return window.DOMAIN;
};

window.PROCESSES_TYPE = {
    '0': 'UNDEFINED',
    '1': '--DELETED--',
    '2': 'PROPERTY_MANAGE',
    '3': 'SEARCH_ORPHANS',
    '4': 'UNDEFINED'
};

window.FRECUENCY = {
    '0': 'frecuency_never',
    '1': 'frecuency_daily',
    '2': 'frecuency_weekly',
    '3': 'frecuency_monthly'
};

window.MAXRUNNINGPROCESSES=0;

window.PROCESSES_STATUS = {
    '0': 'UNDEFINED',
    '1': 'status_applying',
    '2': 'status_complete',
    '3': 'status_error' // finish with error
};

// Sync time in UTC only used day, hours and minuts
window.SYNC_TASK_DAILY = '2015-5-15T07:00:00.000000';
window.SYNC_TASK_WEEKELY = '2015-5-15T08:00:00.000000';
window.SYNC_TASK_MONTHLY = '2015-5-15T09:00:00.000000';
// Withs language js
window.DAYS = {
    '0': 'SUNDAY',
    '1': 'MONDAY',
    '2': 'TUESDAY',
    '3': 'WEDNESDAY',
    '4': 'THURSDAY',
    '5': 'FRIDAY',
    '6': 'SATURDAY'
};

window.DEVELOPER_KEY = 'AIzaSyAUIMEz_i5PHCcqD1oLYpR5OJDUDjLCCQ4';
window.CLIENT_ID = '84979240301-gmeiqtktq8i7djcom9ujp4gmtrljlqrb.apps.googleusercontent.com';
window.SCOPES = ['https://www.googleapis.com/auth/userinfo.email',
                'https://www.googleapis.com/auth/drive.readonly'];

