(function(){
'use strict';

  window.tenantSubmit = function(event, form) {
    event.preventDefault();
    var domainInput = form['domain'];
    var domain = domainInput.value;
    if (domain) {
      location.href = '/a/' + domain + '/admin';
    }
  }

  // to hide menu in smartphones
  window.clickLinkMenu = function(e) {
    var linkEvent = e.target.tagName.match(/li/i) ? e.target : e.target.parentElement;
    var iconMenu = document.querySelector('#icon-menu');
    var links = document.querySelectorAll('#nav li.active');

    // remove active
    for (var i=0; i < links.length; i++) {
      var link = links[i];
      link.classList.remove('active');
    }

    linkEvent.classList.add('active');
    iconMenu.classList.add('unhover');
  };

  window.clickVideo = function(e) {
    e.preventDefault();
    var element = e.target.tagName.match(/img/i) ? e.target.parentElement : e.target;
    var hrefVideo = element.href;
    var videoModal = document.querySelector('#video-modal');
    var videoIframe = document.querySelector('#video-iframe');

    if ( !videoModal ) {
      var videoModal = document.createElement('div');
      videoModal.className = "video-modal";
      videoModal.id = "video-modal";
      videoModal.addEventListener('click', window.closeVideo);
      document.body.appendChild(videoModal);
    }

    if ( !videoIframe ) {
      var videoIframe = document.createElement('iframe');
      videoIframe.className = "video-iframe";
      videoIframe.id = "video-iframe";
      videoIframe.src = hrefVideo;
      videoModal.appendChild(videoIframe);
    }
  }

  window.closeVideo = function(e) {
    var videoModal = document.querySelector('#video-modal');
    videoModal.parentElement.removeChild(videoModal);
  }

  function addEventVideoIcon() {
    var links = document.querySelectorAll('a');
    for (var i=0; i < links.length ; i++ ) {
      var link = links[i];

      if ( link.href && link.href.match(/youtube\.com/)  ) {
        link.addEventListener('click', window.clickVideo, true);
      }
    }
  }

  // click on menu show
  function addEventMenuIcon() {
    var links = document.querySelectorAll('#nav a');
    var iconMenu = document.querySelector('#icon-menu');

    for (var i=0; i < links.length ; i++ ) {
      var link = links[i];
      link.addEventListener('click', window.clickLinkMenu);
    }

    iconMenu.addEventListener('click', function clickIcon(event) {
      iconMenu.classList.remove('unhover');
    });
  }
  
  // load initial function on load page
  document.addEventListener('DOMContentLoaded', function(e) {
    addEventMenuIcon();
    addEventVideoIcon();
  }, false);

})();

