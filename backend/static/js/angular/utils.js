(function() {
    "use strict";
    //for copy once for once propertipes in other object in signature replace tags
       // Extract Dato from date backend format string

    window.extractDate = function(dateString) {
        var date = dateString;
        // Extract ony date
        date = typeof(dateString).match(/string/i) ? date.match(/^[\d-]+/) : '';
        // Replace  numbers for compare with real date
        date = date && date.length ? date[0] : '';

        return date;
    };

    // Compare date
    // If dateToCompare is not required
    window.dateStringIsEqual = function(date, dateToCompare) {
        // valid compare date
        if (dateToCompare && dateToCompare.match(/\d+-\d+-\d+/)) {
            dateToCompare = dateToCompare.match(/\d+/ig);
            var currentTime = new Date(dateToCompare[0], dateToCompare[1], dateToCompare[2]);

        } else {
            var currentTime = new Date();
            // Server is config with UTC
            currentTime = currentTime.getUTCFullYear() + '-' + (currentTime.getUTCMonth() + 1) + '-' + currentTime.getUTCDate();
            // currentTime = currentTime.replace(/-/g,'-0').replace(/-0{2,}/g,'-0'); // add formats numbers
            // to array for add prefix zero
            currentTime = currentTime.match(/\d+/ig);

            currentTime.forEach(function(number ,index, currentTime){
              currentTime[index] = number.length == 1 ? '0' + number : number;
            });

            currentTime = currentTime.join('-');

        }

        var compare = (date === currentTime) ? true : false;

        return compare;

    };

    window.reloadMDLDOM = function($interval, callback) {
        var countTime = 0;
        $interval = setInterval || $interval;

        var timmer = $interval(function() {

            // Reload MDL components
            componentHandler.upgradeDom();

            countTime += 1;

            // Apply callbak function
            if ( countTime >= 7 ) {

                if ( $interval.cancel ) {
                    $interval.cancel( timmer );

                }else{

                    clearInterval( timmer );

                }

                //apply call back
                if (arguments[1]) {
                    callback
                }
            }

        }, 250);
    };
})();
