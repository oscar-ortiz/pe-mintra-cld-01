(function() {
    "use strict";
  
    gApp.service('DriveService', ['$rootScope', '$window', driveService]);

    function driveService($rootScope, $window ) {

        // enable picker in gapi
        $window.gapi.load('picker', {
            'callback': function(data){}
        });

        var service = this;
        service.PIKERVISIBLE = false;

        service.createPicker = function(config, callback) {
            var developerKey = config.developerKey || $window.DEVELOPER_KEY;
            var oauthToken = config.oauthToken || $window.CLIENT_ID;
            var language = config.language || 'en';
            service.PIKERVISIBLE = true;

            function pickerCallback(data) {
                if (data[google.picker.Response.ACTION] == google.picker.Action.PICKED) {
                    callback(data);
                    $rootScope.$apply();
                    service.PIKERVISIBLE = false;
                }
            }

            if (service.PIKERVISIBLE && oauthToken) {
              var viewFolder = new google.picker.DocsView(google.picker.ViewId.DOCS)
                    .setMode(google.picker.DocsViewMode.LIST)
                    .setIncludeFolders(true)
                    .setSelectFolderEnabled(true);

              // DocsViewMode.LIST

              var picker = new google.picker.PickerBuilder().
                  enableFeature(google.picker.Feature.MULTISELECT_ENABLED).
                  enableFeature(google.picker.Feature.MINE_ONLY).
                  addView(viewFolder).
                  setOAuthToken(oauthToken).
                  setDeveloperKey(developerKey).
                  setCallback(pickerCallback).
                  setLocale(language).
                  build();

              picker.setVisible(true);

            }
        };
    }
})();

