(function() {
    "use strict";

    window.gApp = angular.module('gApp', [
        'ngAnimate',
        'ngCookies',
        'mainMenu',
        'settings',
        '$eforModal',
        'esmg.filters',
        '$loadAppWidget',
        '$searchInput',
        'ui-notification',
        'pascalprecht.translate'
    ]);

    gApp.service('EndpointsService', ['$log', '$q', '$rootScope', '$http', '$window', 'requestNotificationChannel', EndpointsService]);
    // From configure.js
    gApp.config(configure);
    gApp.config(notificationConfig);
    gApp.config(translateConfig);
    gApp.run(runMDLObservator);

    // Main controller for general needs
    gApp.controller('mainController', ['$log', '$window', '$rootScope', '$scope', '$translate', '$cookieStore', 'EndpointsService', '$filter', 'Notification',
        function getExpirationDate($log, $window, $rootScope, $scope, $translate, $cookieStore, endpointsService, $filter, notification) {

        var translate = $filter('translate');
        $rootScope.setLanguage = 'es'; //$cookieStore.get('setLanguage') || $window.translateConfig()[0]; // 0 is language navigator

        $scope.$on( endpointsService.ENDPOINTS_READY, function(){
            // Not use authorize method because when is first time load, the signatures not load in load endpoint
            endpointsService.authorize(CLIENT_ID, SCOPES, getSettings);
        });

        $rootScope.login = function() {
            endpointsService.authorize(CLIENT_ID, SCOPES, function(){
              $scope.$apply();
            }, false);
        };

        // Change language
        $rootScope.changeLanguage = function(langKey) {
            var langList = $window.translateConfig()[1]; // ['es','en'] from /static/js/resource/language.js

            if (langKey && langList.lastIndexOf(langKey) > -1) {
                $translate.use(langKey);
                $cookieStore.put('setLanguage', langKey);
                $rootScope.setLanguage = langKey;

            } else {
                var lang = $rootScope.setLanguage || window.translateConfig()[0]; // 'en'
                var langSelected = langList.lastIndexOf(lang); // int
                var langChange = '';
                // select next lang from list
                if (langSelected >= langList.length -1) {
                    langChange = langList[0];
                } else {
                    langChange = langList[langSelected+1];
                }

                // Change language and save cookies
                $translate.use(langChange);
                $cookieStore.put('setLanguage', langChange);
                $rootScope.setLanguage = langChange;
                $rootScope.$digest();
            }
        };

        //$rootScope.changeLanguage($rootScope.setLanguage);

        function getSettings() {

            // Validate if is domain workspace dont work in dashboard
            if ( !location.pathname.match(/^\/?a\//) ) {
                return false;
            }
            // Get endpoint for date
            var data = {
                domain : $window.DOMAIN
            };

            endpointsService.getClientSettings(data, function(response) {
                // Validate count retries
                $scope.retriesLicenseask= $scope.retriesLicenseask ? $scope.retriesLicenseask+1 : 1;

                // Validation error
                if (response.error) {
                    // Error is by authorization
                    if (response.error.code == 503 && $scope.retriesLicenseask < 4) {
                        $log.debug( translate('reloading_license'), response );

                        $window.timeReloadLicese = setTimeout(function() {
                            getExpirationDate();
                        }, 1000);
                    } else {
                        notification.error(translate('reloading_license_fail'));
                    }
                    return false;
                }
                //define frequency allowed in process
                $rootScope.frequency_allowed=response.frequency_process;
                // validation license date in response
                if ( !response.license_expire_date ) {
                    $log.error('license not in response', response);
                    notification.error( translate('reloading_license') );

                    if ($scope.retriesLicenseask < 4) {
                        $window.timeReloadLicese = setTimeout(function() {
                            getExpirationDate();
                        }, 10000);
                    } else {
                        notification.error(translate('reloading_license_fail'));
                    }

                    return false;
                }

                var dateExpiration = response.license_expire_date; //'2016-05-31 22:28:47';
                dateExpiration = $window.extractDate(dateExpiration);
                $rootScope.expiredLicenseDate = dateExpiration;

                // compare expiration time
                var toDay = new Date();
                var expireToString = dateExpiration.match(/\d+/ig);
                // substract 15 days left to original date expiration
                var expirationLeftTime = new Date(expireToString[0], expireToString[1], expireToString[2]-15);

                // compare date format
                if ( toDay > expirationLeftTime  ) {
                    $rootScope.expirationIsNear = true;
                    $log.warn('license expiration time is near');
                }

                // assign langguage from endpoint or browser
                response.language = response.language ? response.language : $rootScope.changeLanguage()[0];
                // Select language when page is loaded
                $rootScope.changeLanguage(response.language);

            });

        }


    }]);

})();
