(function() {
    "use strict";
    
    gApp.controller('adminIndex', ['$log', '$scope', '$rootScope', '$window', '$location', '$interval', 'EndpointsService', 'DriveService','eforModal', 'Notification', '$filter',
    function($log, $scope, $rootScope, $window, $location, $interval, endpointsService, DriveService, $efmodal, notification, $filter) {
        // List variables
        var RELOAD_TIME = 6000; // Usefull for time lapese to reload when is running processes

        $rootScope.DOMAIN = $window.DOMAIN;
        $rootScope.USER = $window.USER_EMAIL;
        $scope.processesList; // [] Contain list process
        $scope.STATUS = $window.PROCESSES_STATUS;
        $scope.PROCESSES_TYPE = $window.PROCESSES_TYPE;
        $scope.processInCreation; // it is used in the modal
        $scope.activeSlide; // Save slide visibe
        $scope.pagesTokens = {}; // save tokens id
        // Task dates from constants
        $scope.dateDaily = $window.SYNC_TASK_DAILY;
        $scope.dateWeekely = $window.SYNC_TASK_WEEKELY;
        $scope.dateMonthly = $window.SYNC_TASK_MONTHLY;
        $scope.processesRunning = []; // This keeping the processess ID for compare
        $scope.enableCreateProcess = true; // This verify max the process running
        $scope.processesFrequencyZero = []; // Process frequency zero
        $scope.TODAY = new Date().toISOString();
        $scope.processInView = undefined; // {}
        var translate = $filter('translate');
        var USER_TYPE = ['anyone', 'user', 'group', 'domain'];
        var PERMISSIONS = ['owner', 'writers', 'commenters', 'viewers', 'none'];
        // Models empty for create new proccess
        var ModelNewProccess = {
            type: undefined,
            name : '',
            status: undefined,
            owner: [],
            new_owner: '',
            folders: [],
            files: [],
            frequency: 0, // 0 is none
            writers: [], // {user_email:'edgar@eforcers.com', type:'user'},
            commenters: [],
            viewers: [],
            none: [], // without any permission
            reset_permissions: false,
            viewer_cancopy: true,
            writer_canshare: true,
            // below properties is only used in view
            searchAs: 1, // type user
            // template for add user permission
            userOrGroupToAdd: {
                user_email: '',
                type: '',
                permission: ''
            },
            actionsGroupsView: {
                'group': [],
                'user': [],
                'domain': []
            },
            showFrequency: false,
            tokenUser: '',
            userQuery: '',
            usersResult: [],
            userOwnerQuery: '',
            usersOwnerResult: [],
            userPermissionQuery: '',
            usersPermissionsResult: [],
            isEdit: false
        };

        // Initialize with authorization
        $scope.$on(endpointsService.ENDPOINTS_READY, function() {
            endpointsService.authorize(CLIENT_ID, SCOPES, initialize);
        });

        function initialize() {
            // Create empty list container processes
            $scope.processesList = [];

            // List
            $scope.listProcess();
        };

        // Get list process
        // TODO: Bind with process endpoints
        $scope.listProcess = function(nextPage) {
            var data = {
                domain : $window.DOMAIN,
                order : '-created',
                limit : '50'
            };
             
            // Validate token page
            if (nextPage) {
                data.pageToken = nextPage;
            }

            endpointsService.listProcess(data, function(response) {
                // Validate response
                if (response.error || response.code >= 400 ) {
                    // Error authorization
                    if (response.error.code == 503) {
                        $log.error(translate('notification_request_authorization'), response );
                        notification.error( translate('notification_request_authorization') );
                        // Recall endpoint with autorization code
                        var reCallEndpoint = function(){ return $scope.listProcess(nextPage) };
                        endpointsService.authorize(CLIENT_ID, SCOPES, reCallEndpoint);
                        return false;
                    }
                    
                    // Other error
                    $log.error( translate('notification_error_on_list_processes'), response );
                    notification.error( translate('notification_error_on_list_processes') );
                    return false;
                }

                // Save nextoken for pagination
                $scope.pagesTokens.cursor = response.nextPageToken || null;

                // Apply respoonse to view model
                $scope.processesList = response.items || [];

                // find for reload status list
                if ( $scope.processesList.length ) {
                    findForStatus( $scope.processesList );
                }

            });

        };

        // Open modal process owner change
        $scope.openCreateProcess = function(type, objectToEdit) {
            // Is edit
            if (objectToEdit) {
                $scope.processInCreation = prepareObjectToEdit(objectToEdit);

                // Flag for changes in view
                $scope.processInCreation.isEdit = true;
                type = $scope.processInCreation.type;

            } else {
                //Copy from empty model
                $scope.processInCreation = angular.copy(ModelNewProccess);
                $scope.processInCreation.type = type;
            }

            // Watch var for manage permisionns switch
            $scope.$watch('processInCreation.searchAs', function(newValue, oldValue) {
                // Reset and clear query selected
                $scope.clearUserPermissionSelected();
                var input = $('input[ng-model="scope.processInCreation.userPermissionQuery"]');
                
                // Is domain
                if (newValue == 3) {
                    $scope.setUsersPermissions($rootScope.DOMAIN, objectToEdit);
                    disableInput(input, true);
                } else {
                    disableInput(input, false);
                }
            });

            $scope.$watch('processInCreation.searchAs', function(newValue, oldValue) {
                // Reset and clear query selected
                $scope.clearUserPermissionSelected();
                var input = $('input[ng-model="scope.processInCreation.userPermissionQuery"]');
                
                // Is domain
                if (newValue == 3) {
                    $scope.setUsersPermissions($rootScope.DOMAIN);
                    disableInput(input, true);
                } else {
                    disableInput(input, false);
                }
            });
            
            // When no folders selected theprocess is not posibble assign frecuency
            $scope.$watch('processInCreation.folders', function(newValue, oldValue) {
                if ( oldValue.length && !newValue.length ) {
                    $scope.processInCreation.showFrequency = false;
                }
            });

            // Open modal type
            if ( type == 1 ) {
                // TODO: Verify if its current
                $efmodal.open({templateUrl:'/static/html/modals/change_owner.html', scope:$scope});
            } else if ( type == 2 ) {
                $efmodal.open({templateUrl:'/static/html/modals/manager_permissions.html', scope:$scope});
            } else if ( type == 3 ) {
                $efmodal.open({templateUrl:'/static/html/modals/search_orphans.html', scope:$scope});
            }
        };
        
        // open modal from view
        $scope.openDetails = function(process) {
                            $scope.processInView = process;
                $scope.processInView.total_processed = 0;
                // Assign name
                $scope.processInView.name = $scope.processInView.name || translate($window.PROCESSES_TYPE[$scope.processInView.type]);


               if($scope.processInView.details){
                    try{
                        $scope.processInView.proccessDetail = JSON.parse($scope.processInView.details);
                        $scope.processInView.total_processed = $scope.processInView.proccessDetail.length;
                    } catch(error){
                        $scope.processInView.proccessDetailPlane = $scope.processInView.details;
                    }
               }

            if(process.type == 2){
                $efmodal.open({templateUrl:'/static/html/modals/details_process_permissions.html', scope:$scope});

            } else {
                $efmodal.open({templateUrl:'/static/html/modals/details_process_orphans.html', scope:$scope});

            };
        };

        // Edit process
        $scope.openEdit = function(process) {
            // Opden Edit mode
            $scope.openCreateProcess(process.type, process);
        };

        // Redirect find user or group
        $scope.getUsersGroupByFind = function(){
            // Search in users
            var query = $scope.processInCreation.userPermissionQuery;
            
            // data for endpoints
            var data = {
                domain: $rootScope.DOMAIN,
                prefix: query
            };

            // Find user|groups|domain {1:users, 2:groups, 3:domain}
            if ( $scope.processInCreation.searchAs == 1 ) {
                   // Connect to endpoint
                endpointsService.listDriveUsers(data, function(response) {
                    // Validate response
                    if (response.error || response.code >= 400 ) {
                        // Error authorization
                        if (response.error.code == 503) {
                            $log.error(translate('notification_request_authorization'), response );
                            notification.error( translate('notification_request_authorization') );
                            // Recall endpoint with autorization code
                            endpointsService.authorize(CLIENT_ID, SCOPES, $scope.getUsersByFind);
                            return false;
                        }
                        
                        // Other error
                        $log.error(translate('notification_error_on_search_users'), response );
                        notification.error( translate('notification_error_on_search_users') );
                        return false;
                    }

                    // Validate resul = 0 or undefined
                    if (response.items < 1 || !response.items) {
                        $log.info(translate('notification_users_not_found'), $scope.processInCreation.userQuery );
                        notification.info( translate('notification_users_not_found') );
                        return false;
                    }
                    
                    // Write response items
                    $scope.processInCreation.usersPermissionsResult = response.items;
                    
                });

            } else if ( $scope.processInCreation.searchAs == 2 ) {
                // Connect to endpoint
                endpointsService.listDriveGroups(data, function(response) {
                    // Validate response
                    if (response.error || response.code >= 400 ) {
                        // Error authorization
                        if (response.error.code == 503) {
                            $log.error(translate('notification_request_authorization'), response );
                            notification.error( translate('notification_request_authorization') );
                            // Recall endpoint with autorization code
                            endpointsService.authorize(CLIENT_ID, SCOPES, $scope.getUsersByFind);
                            return false;
                        }
                        
                        // Other error
                        $log.error(translate('notification_error_on_search_groups'), response );
                        notification.error( translate('notification_error_on_search_groups') );
                        return false;
                    }

                    // Validate resul = 0 or undefined
                    if (response.items < 1 || !response.items) {
                        $log.info(translate('notification_groups_not_found'), $scope.processInCreation.userQuery );
                        notification.info( translate('notification_groups_not_found') );
                        return false;
                    }
                    
                    // Write response items
                    $scope.processInCreation.usersPermissionsResult = response.items;
                                        
                });
            
            // ommited for domain selected
            } else if ( $scope.processInCreation.searchAs != 3 ) {
                notification.warning(translate('notification_not_user_type_selected'));
            }
        };

        // Create process for Owbership change
        $scope.getUsersByFind = function(modelToShow) {
            // Select scope for slide 0 or 1
            var query = $scope.activeSlide == 0 ? $scope.processInCreation.userQuery : $scope.processInCreation.userOwnerQuery;
            // data for endpoints
            var data = {
                domain: $rootScope.DOMAIN,
                prefix: query
            };

            // Connect to endpoint
            endpointsService.listDriveUsers(data, function(response) {
                // Validate response
                if (response.error || response.code >= 400 ) {
                    // Error authorization
                    if (response.error.code == 503) {
                        $log.error(translate('notification_request_authorization'), response );
                        notification.error( translate('notification_request_authorization') );
                        // Recall endpoint with autorization code
                        endpointsService.authorize(CLIENT_ID, SCOPES, $scope.getUsersByFind);
                        return false;
                    }
                    
                    // Other error
                    $log.error(translate('notification_error_on_search_users'), response );
                    notification.error( translate('notification_error_on_search_users') );
                    return false;
                }

                // Validate resul = 0 or undefined
                if (response.items < 1 || !response.items) {
                    $log.info(translate('notification_users_not_found'), $scope.processInCreation.userQuery );
                    notification.info( translate('notification_users_not_found') );
                    return false;
                }
                
                // Filter slide section 
                if ( $scope.activeSlide == 0  ) {
                    // Response succesfull
                    $scope.processInCreation.usersResult = response.items;
                } else if ( $scope.activeSlide == 1 ) {
                    // Response to user
                    $scope.processInCreation.usersOwnerResult = response.items;
                }
            });
        };

        // Validate all form
        $scope.validateOwnerShipChange = function() {
            // Validation for assign permissions
            if ( $scope.activeSlide == 0  && $scope.processInCreation.type == 2 ) {
                // Validation files selected
                if (!$scope.processInCreation.folders.length && !$scope.processInCreation.files.length ) {
                    notification.error(translate('notification_no_docs_selected'));
                    return false;
                }
                // Go next slide
                $scope.goSlide('next');

            // Validation for orphans files
            } else if ( $scope.processInCreation.type == 3 && $scope.activeSlide == 0 ) {
                // Validation if users is selecteds
                if ( !$scope.processInCreation.owner || $scope.processInCreation.owner.length == 0 ) {
                    notification.error(translate('notification_not_users_selected'));
                    return false;
                }

            } else if ( $scope.processInCreation.type == 2 && 1 && $scope.activeSlide == 1 ) {
                    // Prevent unexists name
                    $scope.processInCreation.actionsGroupsView.user = $scope.processInCreation.actionsGroupsView.user || [];
                    var valid = $scope.processInCreation.actionsGroupsView.user.length;
                    // TODO: verificate exists
                    $scope.processInCreation.actionsGroupsView.user = $scope.processInCreation.actionsGroupsView.user || [];
                    $scope.processInCreation.actionsGroupsView.group = $scope.processInCreation.actionsGroupsView.group || [];
                    $scope.processInCreation.actionsGroupsView.domain = $scope.processInCreation.actionsGroupsView.domain || [];
                    // validation
                    valid = valid || $scope.processInCreation.actionsGroupsView.group.length;
                    valid = valid || $scope.processInCreation.actionsGroupsView.domain.length;
                    valid = valid || $scope.processInCreation.new_owner;

                    if (!valid) {
                        notification.error(translate('create_no_permissions_assigned'));
                        return false;
                    }

                // go next slide
                $scope.goSlide('next');

            } else if ( $scope.activeSlide == 2 ) {
                // Validate if showFrequency is selected
                if ($scope.processInCreation.showFrequency) {

                    // Validate folders select
                    if (!$scope.processInCreation.folders.length) {
                        notification.error(translate('notification_no_folder_selected'));
                        return false;
                    }

                    // Validate name for process
                    $scope.processInCreation.name = $filter('validName')($scope.processInCreation.name);
                    if (!$scope.processInCreation.name) {
                        notification.error(translate('notification_no_name_process_selected'));
                        return false;
                    }

                    // Validate frecuence selected
                    if (!$scope.processInCreation.frequency) {
                        notification.error(translate('notification_no_period_selected'));
                        return false;
                    }
                }
            }
            // Success
            return true;

        };

        // Clear user selected for GoogleDrive
        $scope.clearUserSelected = function() {
            $scope.processInCreation.userQuery = '';
            $scope.processInCreation.owner = [];
            $scope.processInCreation.tokenUser = '';
            $scope.processInCreation.files = [];
            $scope.processInCreation.folders = [];
            // Enable inputSearch
            var input = $('input[ng-model="scope.processInCreation.userQuery"]');
            disableInput(input, false);
        };

        // Select new owner for files selected
        $scope.setUserOwner = function(user) {
            // Response successfull
            $scope.processInCreation.new_owner = user;
            $scope.processInCreation.userOwnerQuery = user;
            // Disable inputSearch
            var input = $('input[ng-model="scope.processInCreation.userOwnerQuery"]');
            disableInput(input,true);
        };
        
        // Select new owner for files selected
        $scope.setUserForOrphansFiles = function(newUser) {
            // Validate exists
            $scope.processInCreation.owner = $scope.processInCreation.owner || [];

            // Prevent Exists user in group
            if ( $scope.processInCreation.owner.indexOf(newUser) > -1 ) {
                notification.info(translate('notification_user_already_exists'));
                // Clean resuslt
                $scope.processInCreation.usersResult = [];

                return false;
            }

            $scope.processInCreation.owner.unshift(newUser);

            // Clear input search
            $scope.clearInputUser();
        };

        // Clear for ouser in orphans files
        $scope.clearInputUser = function() {
            $scope.processInCreation.userQuery = '';
            $scope.processInCreation.usersResult = [];
        };

        // Remove from list
        $scope.userSustractObject = function(removeUser) {

            // Get index in array
            var userIndex = $scope.processInCreation.owner.indexOf(removeUser);

            // Validate exist
            if(userIndex >= 0) {
                $scope.processInCreation.owner.splice(userIndex, 1);
            }
        };

        // Clear user selected permissions user query
        $scope.clearUserOwnerSelected = function() {
            $scope.processInCreation.new_owner = '';
            $scope.processInCreation.userOwnerQuery = '';
            // Enable inputSearch
            var input = $('input[ng-model="scope.processInCreation.userOwnerQuery"]');
            disableInput(input, false);
        };

        // Select user or group for pass to list
        $scope.setUsersPermissions = function(userOrGroup, objectToEdit) {
            // Validate is editing
            $scope.processInCreation = $scope.processInCreation;
            var isDomain = userOrGroup.match('@') ? false : true;
            
            // Validate if email already exist and is not editing for prevent notifications
            if ( !objectToEdit && emailExistsInGroups(userOrGroup) ) {
                if (isDomain) {
                    notification.warning(translate('notification_domain_already_add'));
                } else {
                    // filter message notification user or group
                    if ( $scope.processInCreation.searchAs == 2 ) {
                        notification.warning(translate('notification_group_already_exists'));
                    } else {
                        notification.warning(translate('notification_user_already_exists'));
                    }
                }

                return false;
            }


            // Extract type from radio buttons
            var type = $scope.processInCreation.searchAs;
            // Rewrite input query with all user_email
            $scope.processInCreation.userPermissionQuery = userOrGroup;
            // Prevetn error for object
            $scope.processInCreation.userOrGroupToAdd = $scope.processInCreation.userOrGroupToAdd || {};
            // Assign email to user object
            $scope.processInCreation.userOrGroupToAdd.user_email = userOrGroup;
            // Assign type to user object
            $scope.processInCreation.userOrGroupToAdd.type = USER_TYPE[type];
            // Disable inputSearch 
            var input = $('input[ng-model="scope.processInCreation.userPermissionQuery"]');
            disableInput(input, true);

        };

        /*
         * Add user
         *@manualAddObject: Is only when prepare in edit
         */
        $scope.addUserToManagePermission = function( manualAddObject ) {
            // Get peermission from user for add
            var userPermission = $scope.processInCreation.userOrGroupToAdd.permission;

            // Validate if permision is selected
            if (!manualAddObject && !userPermission) {
                notification.info(translate('notification_not_user_selected'));
                $log.log('EDMG: Error on validate user to add:', userToAdd, userPermission);
                return false; 
            }
            
            // Validate if permision is for owner or not
            if (userPermission == 'new_owner') {
                $scope.processInCreation.new_owner = $scope.processInCreation.userOrGroupToAdd.user_email;

            } else {
                // Prepare object to push
                var userToAdd = {
                    type: $scope.processInCreation.userOrGroupToAdd.type,
                    user_email: $scope.processInCreation.userOrGroupToAdd.user_email,
                    permission: userPermission // Used in view
                };

                // Add to view and model to send and validate
                addUserToActionGroup(userToAdd, userPermission);
            }

            // Clear input
            $scope.clearUserPermissionSelected();

        };

        // Remove to ActionGroup
        $scope.removeUserToActionGroup = function(removeUser) {
            var removeUserCopy = angular.copy(removeUser);

            // Validate if removeUser is Owner (string)
            if ( (typeof(removeUserCopy)).match(/string/i) ) {
                // Empty user_owner
                $scope.processInCreation.new_owner = '';
            
            } else {
                // Get permissions
                var userPermission = removeUserCopy.permission;
                
                // Remove from model to send
                $scope.processInCreation[userPermission] = $scope.processInCreation[userPermission]
                    .filter( function(element) {
                        return element.user_email == removeUserCopy.user_email ? false : true;
                    });
            
                // Remove from view
                $scope.processInCreation.actionsGroupsView[removeUserCopy.type] = $scope.processInCreation.actionsGroupsView[removeUserCopy.type]
                    .filter( function(element) {
                        return element.user_email == removeUserCopy.user_email ? false : true;
                    });
            }

        };

        // Update user permission
        $scope.updateUserPermissions = function(userToUpdate, newPermission) {
            // Get permissions
            var oldPermission = userToUpdate.permission;
            
            // update permission in view groups
            userToUpdate.permission = newPermission;
        
            // Find and update permission
            for (var i=0; i <= $scope.processInCreation[oldPermission].length; i++) {
                
                // Get user iteration
                var userInModel = $scope.processInCreation[oldPermission][i];
                // Validate equality and update permission
                if (userToUpdate.user_email == userInModel.user_email) {
                    // Update permission in view groups
                    userInModel.permission = newPermission;
                    // Remove from old permission group
                    $scope.processInCreation[oldPermission].splice(i,1);
                    // pasete user in new permission group
                    $scope.processInCreation[newPermission].push(userInModel);
                    break;
                }
            }

        };

        // Clear user selected owner
        $scope.clearUserPermissionSelected = function() {
            $scope.processInCreation.userPermissionQuery = '';
            // Clear from copy model
            $scope.processInCreation.userOrGroupToAdd =angular.copy( ModelNewProccess.userOrGroupToAdd);
            // Clear searched result
            $scope.processInCreation.usersPermissionsResult = [];
            // Enable inputSearch
            var input = $('input[ng-model="scope.processInCreation.userPermissionQuery"]');
            disableInput(input, false);
        };

        // Set as as user selected and use token from backend
        $scope.getTokenUser = function(user, newObject) {
            var data = {
                domain: $rootScope.DOMAIN,
                emailUser: user 
            };

            endpointsService.obtainAccessToken(data, function(response) {

                // is editing
                $scope.processInCreation == newObject || $scope.processInCreation;

                // Validate response
                if (response.error || response.code >= 400 ) {
                    // Error authorization
                    if (response.error.code == 503) {
                        $log.error(translate('notification_request_authorization'), response );
                        notification.error( translate('notification_request_authorization') );

                        // Recall endpoint with autorization code
                        var reCallEndpoint = function(){ return $scope.getTokenUser(user) };
                        endpointsService.authorize(CLIENT_ID, SCOPES, reCallEndpoint);
                        return false;
                    }
                    
                    // Other error
                    $log.error(translate('notification_error_on_ask_token'), response );
                    notification.error( translate('notification_error_on_ask_token') + ': ' + user );
                    return false;
                }

                // Validate resul = 0 or undefined
                if (response.items < 1 || !response.items) {
                    $log.info(translate('notification_not_request_token'), $scope.processInCreation.userQuery );
                    notification.info( translate('notification_not_request_token') );
                    return false;
                }

                // Response successfull
                $log.info('Token ' + user + ':', response);
                $scope.processInCreation.owner[0] = user;
                $scope.processInCreation.userQuery = user;

                // Is token that uses Drive picker 
                $scope.processInCreation.tokenUser = response.items;
                $scope.processInCreation.usersResult = [];

                // Disable inputSearch
                var input = $('input[ng-model="scope.processInCreation.userQuery"]');
                disableInput(input, true);

            });
        };

        // Send all info to endpoint for create process
        $scope.createProcessOwnner = function() {
            if ( $scope.validateOwnerShipChange() ) {
                var data = angular.copy( $scope.processInCreation );

                // Change status for prepare backend index and review the most frequent processes
                data.status = 4;

                // Frequency
                data.frequency = data.showFrequency ? data.frequency : 0;
                
                // Name
                data.name = data.showFrequency ? data.name : '' ;

                // Prepare to update info
                if ( data.isEdit ) {
                    data = deleteConfictPropertiesToSave(data);
                    // Change data status
                    data.status = 2;
                }

                // Send to backend
                endpointsService.saveProcessCreated(data, function(response) {
                    // Validate response
                    if (response.error || response.code >= 400 ) {
                        // Error authorization
                        if (response.error.code == 503) {
                            $log.error(translate('notification_error_on_save'), response );
                            notification.error( translate('notification_error_on_save') );
                            // Recall endpoint with autorization code
                            endpointsService.authorize(CLIENT_ID, SCOPES, $scope.getUsersByFind);
                            return false;
                        }
                        // Other error
                        $log.error(translate('notification_error_on_ask_token'), response );
                        notification.error( translate('notification_error_on_ask_token'));
                        return false;
                    }

                    if(response.status==1 || data.isEdit){
                        $log.info('Finish save:', response)

                        // Add to reload list
                        findForStatus([response]);

                        // sync with endpoints
                        if ( data.isEdit ) {
                            updateProcess( response );
                        } else {
                            $scope.processesList.unshift( response );
                        }

                        // close modal
                        $efmodal.close();

                    }

                    if(response.status==2 && !data.isEdit){
                        modalMaxProcceses(1);
                    }
                    if(response.status==4 && !data.isEdit){
                        modalMaxProcceses(2);
                    }

                });
            }
        };

        // Launch to run
        $scope.launchToRun = function(proccesToRun) {

            var data = angular.copy( proccesToRun );
            data = deleteConfictPropertiesToSave(data);
            // Change data status to run
            data.status = 1;

            // Send to backend
            endpointsService.saveProcessCreated(data, function(response) {
                // Validate response
                if (response.error || response.code >= 400 ) {
                    // Error authorization
                    if (response.error.code == 503) {
                        $log.error(translate('notification_error_on_running'), response );
                        notification.error( translate('notification_error_on_running') );
                        // Recall endpoint with autorization code
                        endpointsService.authorize(CLIENT_ID, SCOPES, $scope.getUsersByFind);
                        return false;
                    }
                    // Other error
                    $log.error(translate('notification_error_on_ask_token'), response );
                    notification.error( translate('notification_error_on_ask_token'));
                    return false;
                }

                // Log run
                $log.info('Runing process:', response)
                if(response.status==1) {
                   notification.error( response.name + ': ' + translate('notification_run_process'));
                }
                if(response.status==2){

                   modalMaxProcceses(1);

                }
                // Add to reload list
                findForStatus([response]);

                // sync with endpoints
                updateProcess( response );

            });
        };

        $scope.confirmDelete  = function(proccesToDelete){

              $efmodal.confirm({
                subtitle: translate('msn_delete_process'),
                confirm_text: translate('continue'),
                cancel_text: translate('cancel'),
                confirmAction: function() {
                    $scope.launchToDelete(proccesToDelete)
                },
                cancelAction: function() {
                }
            });

        };

        // delete process
        $scope.launchToDelete = function(proccesToDelete) {


            // Send to backend
            endpointsService.deleteProcessid(proccesToDelete, function(response) {
                // Validate response
                if (response.error || response.code >= 400 ) {
                    // Error authorization
                    if (response.error.code == 503) {
                        $log.error(translate('notification_error_on_delete'), response );
                        notification.error( translate('notification_error_on_delete') );
                        // Recall endpoint with autorization code
                        endpointsService.authorize(CLIENT_ID, SCOPES, $scope.getUsersByFind);
                        return false;
                    }
                    // Other error
                    $log.error(translate('notification_error_on_ask_token'), response );
                    notification.error( translate('notification_error_on_ask_token'));
                    return false;
                }

                // Log delete
                $log.info('Delete process:', response);

                notification.error( response.name + ': ' + translate('notification_delete_process'));

                removeProccessFromViewList( response.id );
                // Add to reload list
                findForStatus([response]);

                // sync with endpoints
                updateProcess( response );

            });
        };


        /*
        * Reports maximum exceeded processes allowed
        */
        function modalMaxProcceses(msn) {

            switch(msn){
                case 1:
                msn='notification_max_run_process';
                break;

                case 2:
                msn='notification_max_process_frequent';
                break;
            }

            $efmodal.confirm({
              subtitle: translate(msn),
              confirm_text: translate('continue')
            });
        };


        // assign permission to user
        $scope.selectPermission = function(type, user) {
            if (user) {
                // move to another group
                $scope.processInCreation.userOrGroupToAdd.permission = PERMISSIONS[type];
            } else {
                $scope.processInCreation.userOrGroupToAdd.permission = PERMISSIONS[type];
            }
        };

        // pagination controller
        $scope.goToPage = function(contrl) {
            /* Pagination select from pane active*/
            var paginationUse = $scope.pagesTokens;
            /* obtain controller 'next'/'back'/'home'*/
            var pageToken;

            switch (contrl) {
                case "home":
                    paginationUse.cursorBack = [];
                    paginationUse.cursorCurrent = null;
                    break;

                case "next":
                    if (paginationUse.cursorCurrent != null) {
                        paginationUse.cursorBack = paginationUse.cursorBack || [];
                        paginationUse.cursorBack.push(paginationUse.cursorCurrent);
                    }

                    paginationUse.cursorCurrent = paginationUse.cursor;
                    pageToken = paginationUse.cursor;
                    break;

                case 'current':
                    pageToken = paginationUse.cursorCurrent;
                    break;

                case "back":
                    paginationUse.cursorBack = paginationUse.cursorBack || [];
                    paginationUse.cursorCurrent = paginationUse.cursorBack.pop();
                    pageToken = paginationUse.cursorCurrent;
                    break;
            }

            $scope.listProcess(pageToken);
        };

        // UTILS
        
        // Validate for add user o group
        function emailExistsInGroups(userEmail) {
            var exist = false;
            var groups = ['writers', 'commenters', 'viewers', 'none'];
            // iteration in all permissions
            main: for ( var i=0; i < groups.length; i++) {
                // get group 
                var group = groups[i];

                // Find user in lists
                for ( var j=0; j < $scope.processInCreation[group].length; j++ ) {
                    // Get user
                    var iterUser = $scope.processInCreation[group][j];

                    // Validation user
                    if (iterUser.user_email == userEmail) {
                        exist = true;
                        break main;
                    }
                }
            }


            if ( userEmail == $scope.processInCreation.new_owner) {
                exist = true;
            }

            // Not exist
            return exist;
        };


        /*
         * Endpoint return error when someones properties ares self that enpoints autocomplete
         * @processData: is proccess object
         * return  new object data
         */
        function deleteConfictPropertiesToSave(processData) {
            var data = processData;
            delete data.created;
            delete data.errors_files;
            delete data.errors_folders;
            delete data.files_processed;
            delete data.folders_processed;
            delete data.listed_files;
            delete data.listed_folders;

            return data;
        };


        /* Add to Actiongroup
         *@newObject: is sended when is in editing process
         */
        function addUserToActionGroup(userToAdd, userPermission, newObject) {
            // Validate user selected if not is to editing
            if (!newObject) {
                if (!userToAdd || !userToAdd.user_email) {
                    notification.info(translate('notification_not_user_selected'));
                    $log.log('EDMG: Error on validate user to add:', userToAdd, userPermission);
                    return false; 
                }

                // Validate permission selected
                if (!userPermission) {
                    notification.info(translate('notification_not_user_permission_selected'));
                    $log.log('EDMG: on validate permission to add:', userToAdd, userPermission);
                    return false; 
                }

                // Add to group actions
                if (!$scope.processInCreation[userPermission]) {
                    $log.warn(translate('type permission not exist:', userPermission));
                    return false;
                }
            }

            // Add user to permisson group Model if is not editing
            if (!newObject) {
                $scope.processInCreation[userPermission].push(userToAdd);
            }

            // Add to view user|group|domain
            if (userToAdd.type) {
                // Replace object if is editing
                $scope.processInCreation = newObject || $scope.processInCreation;
                
                // Prevetn error if not exist any method
                $scope.processInCreation.actionsGroupsView = $scope.processInCreation.actionsGroupsView || {};
                $scope.processInCreation.actionsGroupsView[userToAdd.type] = $scope.processInCreation.actionsGroupsView[userToAdd.type] || [];

                // Add user to permission group
                $scope.processInCreation.actionsGroupsView[userToAdd.type].push(userToAdd);

                // if not is editing because prevent notification when is open to editing
                if (!newObject && userToAdd.type == 'domain') {
                    $scope.processInCreation.searchAs = 2;
                    $scope.processInCreation.searchAs = 3;
                }
            }

            return  $scope.processInCreation;

        };


        /*
         * Find and update status from processes response on request staus
         * @param {Array} processesResponseObjects - Is array complete objects form endpoints
         * @return {undefined} - No return elements
        */
        function findForStatus(processesResponseObjects) {

            // Valid exists of processRuning
            var processesRunning = $scope.processesRunning ? angular.copy($scope.processesRunning) : [];
            var reloadList = false; // this is for reload all list or not
            var processesResponseObjects= processesResponseObjects || []; // Preassign for prevent errors

            /* This var "completedList" save a copy of runningProccess for delete each if is returning,
            // When finish iteration this ids was take as succesfull finished */
            var completedList = $scope.processesFrequencyZero ? angular.copy($scope.processesFrequencyZero) : [];

            // Iteration in new process to compare status
            responseProcessesIter: for ( var i=0; i < processesResponseObjects.length; i++ ) {
                var processResponse = processesResponseObjects[i];

                //* Save in runing process first time, 1 == applying
                if ( processesRunning.length == 0 && processResponse.status == 1 ) {
                    $scope.processesRunning.push( processResponse.id );
                    if(processResponse.frequency==0){
                    $scope.processesFrequencyZero.push(processResponse.id)
                    }
                    continue;
                }

                if(i>0){
                    if(processesResponseObjects[i].id == processesResponseObjects[i-1].id){
                        continue;
                    }
                }

                processesRunning = processesRunning.filter(function(elem, pos) {
                return processesRunning.indexOf(elem) == pos;
                });

                // Search in list runing
                processRunIter: for ( var j=0; j < processesRunning.length; j++ ) {
                    var processRunningList = processesRunning[j];

                    // Is self process object?
                    if ( processResponse.id == processRunningList ) {

                        // Remove from finished
                        completedList = completedList.filter(function( completesId ) {
                            return completesId != processResponse.id? true : false;
                        });

                        // Get name or process type name
                        var processName = processResponse.name || translate($window.PROCESSES_TYPE[processResponse.type]);

                        // Compare status changes
                        if ( processResponse.status == 2 && $scope.processesRunning.length > 0 ) {
                            if(processResponse.frequency!=0){
                                // Notification Success
                                notification.info( translate('notification_process_finish_ok' ) + ': ' + processName );
                                reloadList = true;}
                            else{
                                completedList.push(processResponse.id)
                            }

                            // Remove process
                            removeProcessInProcessesRunning(processResponse.id);

                            // update process
                            updateProcess(processResponse);
                        } else if ( processResponse.status == 3 && $scope.processesRunning.length > 0 ) {
                            // Notification Error
                            notification.info( translate('notification_process_finish_error' ) + ': ' + processName );
                            reloadList = true;

                            // Remove process
                            removeProcessInProcessesRunning(processResponse.id);
                        }

                        // Update process
                        updateProcess(processResponse);

                        // Break this son bucle
                        break processRunIter;

                    } else if ( j == processesRunning.length -1 && processResponse.status == 1 ) {
                        // If process is runing and is last
                        // add procces to list in runing 
                        $scope.processesRunning.push(processResponse.id);
                        if(processResponse.frequency==0){
                            $scope.processesFrequencyZero.push(processResponse.id)
                        }
                    }
                }
            }

            // When all proccess is finished launch notifications finished items
            if ( completedList.length && $scope.processesRunning.length > 0 ) {
                for ( var k=0; k < completedList.length; k++ ) {
                    var idSuccess = completedList[k];
                    var deletededProcess = removeProccessFromViewList( idSuccess );
                    removeProcessInProcessesRunning( idSuccess );
                    // send notification
                    if ( deletededProcess ) {
                        notification.info(translate($window.PROCESSES_TYPE[deletededProcess.type])
                                + ': ' + translate('notification_process_finish_ok_and_move'));
                    }

                    for ( var t=0; t < $scope.processesFrequencyZero.length; t++ ) {

                        if(completedList[k]==$scope.processesFrequencyZero[t]){
                            $scope.processesFrequencyZero.splice(t,1)
                        }
                    }
                }

            }

            //Verify number of process for enable/disable creation of process
            VerifyNumberProcess($scope.processesRunning)

            // Programing timming for ask again
            if ( $scope.processesRunning.length ) {
                // Validation page change
                timerReloadProcessesStatus();
            }
        };

        function VerifyNumberProcess(processrunning) {

            if (processrunning.length>=$window.MAXRUNNINGPROCESSES){
                $scope.enableCreateProcess=false;
            }
            if (processrunning.length<$window.MAXRUNNINGPROCESSES){
                $scope.enableCreateProcess=true;
            }

        };
        
        /*
         * Update single object model in angular
         * @param {Object} processUpdated - Is a  of process objects
         * @return {boolean} toReturn - confirm status
        */
        function updateProcess(processUpdated) {
            var toReturn = false;

            // Search in proccess list in view
            for ( var i=0; i < $scope.processesList.length; i++ ) {

                // Match processes
                if ( $scope.processesList[i].id ==  processUpdated.id ) {
                    // the index number is needed for angular update model
                    $scope.processesList[i] = processUpdated;
                    
                    // stop and confirm updated
                    toReturn = true;
                    break;
                }
            }

            // Update angular models
            $scope.$apply();
            return toReturn;
        };

        /**
         * Remove proccess to the list view
         * @param {String} idProcess
         * @return {Object} processDeleted
         */
        function removeProccessFromViewList( idProcess ) {
             var processDeleted;
             var indexProcessToDelete;

             // Search inde object for delete
             for (var i=0; i < $scope.processesList.length; i++) {
                 var processInView = $scope.processesList[i];
                 
                 if (processInView.id == idProcess) {
                     indexProcessToDelete = i;
                     processDeleted = processInView;
                     break;
                 }
             }

             $scope.processesList.splice(indexProcessToDelete, 1);
             // Return finded
             return processDeleted;
        }

        /*
         * @procceseRemoveId: String
         * Remove id in array running process
        */
        function removeProcessInProcessesRunning(procceseRemoveId) {
            // Validate
            if (!procceseRemoveId) {
                $log.info('not process for remove in processRuning')
                return false;
            }

            // Remove
            $scope.processesRunning = $scope.processesRunning.filter(function(process) {
                return process == procceseRemoveId ? false : true;
            });

            $scope.$apply();
        }

        // validate and answer for changes in process staurs
        function timerReloadProcessesStatus() {
            /* FIX TODO: Prevent multiple tiger*/
            $window.clearInterval($window.updateActives);
            delete $window.updateActives;

            $window.updateActives = $window.setInterval( function() {
                endpointsService.listProcessesByIds({'ids': $scope.processesRunning}, function(response) {

                    if (response.error) {
                        $log.error('error on reload actives process', response);

                        /* Validation oauth token*/
                        if ( response.code === 401 && response.message.match(/token/i) ) {
                            notification.error(translate('reloading_token'));
                            endpointsService.authorize(CLIENT_ID, SCOPES, $scope.listProcess);
                        }

                        return false;
                    }

                    /* Reload for verify new process change status*/
                    $window.clearInterval($window.updateActives);
                    delete $window.updateActives;

                    // Ask status after 6s
                    findForStatus(response.items);

                });
            }, RELOAD_TIME ); // Seconds for reload in runing processes
        };

        // Complete and adjust all object to load in view
        function prepareObjectToEdit(objectToPrepare) {

            // Vars to use
            var modelNewProccess = angular.copy(ModelNewProccess);
            var objectKeys = Object.keys(modelNewProccess);
            var newObject = angular.copy(objectToPrepare);

            // Copy properties
            for ( var i=0; i < objectKeys.length; i++ ) {
                // overwrite type object
                if ( newObject[objectKeys[i]] == undefined ) {
                    newObject[objectKeys[i]] = modelNewProccess[objectKeys[i]];
                }

                if ( objectKeys[i] == 'owner' ) {
                    // Token for admin permissions
                    if (newObject.type == 2) {
                        // Complete query selection
                        newObject.userQuery = newObject[objectKeys[i]][0]; 
                        $scope.getTokenUser(newObject.userQuery, newObject);
                    }
                }

                // Add user Permission to table model view
                if ( objectKeys[i].match(/^(viewers)|(commenters)|(writers)|(none)$/) ) {
                    for (var j=0; j < newObject[objectKeys[i]].length; j++ ) {

                        // Prepare object to push
                        var userToAdd = {
                            type: newObject[objectKeys[i]][j].type,
                            user_email: newObject[objectKeys[i]][j].user_email,
                            permission: objectKeys[i] // Used in view
                        };

                        // Add to view and model to send and validate
                        addUserToActionGroup(userToAdd, objectKeys[i], newObject);
                    }
                }

                // To open frequency options
                if ( objectKeys[i] == 'frequency' ) {
                    newObject.frequency = parseInt(newObject.frequency);
                    newObject.showFrequency = newObject.frequency ? true : false;
                }

                if ( objectKeys[i] == 'type') {
                    newObject.type = parseInt(newObject.type);
                }
            }

            return newObject;
        }


        // disable any input (in this case disable search users in modal for assign permissions)
        function disableInput(input, action) {
            if (action) {
                input.attr('disabled', 'disabled');
            } else {
                input.removeAttr('disabled');
            }
        }

        // Open Picker GoogleDrive
        $scope.openDrive = function() {
            DriveService.createPicker({
                    oauthToken: $scope.processInCreation.tokenUser,
                    language: $rootScope.setLanguage
                },
                function(data) {
                    $log.info('files selected: ', data);
                    var docs = data.docs;
                    if (docs && docs.length) {
                        splitDocs(docs);
                    }
            } );
        };

        // Divide foders and files from GoogleDrive respons
        function splitDocs(newDocs) {
            // validate if previsuly exists docs selected
            var foldersActual = $scope.processInCreation.folders || [];
            var filesActual = $scope.processInCreation.files || [];
            var newFolders = [];
            var newFiles = [];

            // Split folders and files
            for (var i=0; i  < newDocs.length; i++) {
                var doc = newDocs[i];
                // add icon for save in endpoint
                doc.icon = doc.iconUrl;
                doc.parent_id = doc.parentId;
                if (doc.type == 'folder'){
                    newFolders.push(doc);
                } else {
                    newFiles.push(doc);
                }
            }

            // Filter if exists or is repeat
            newFolders = addDocs(newFolders, foldersActual);
            newFiles = addDocs(newFiles, filesActual);

            $scope.processInCreation.folders = newFolders.concat($scope.processInCreation.folders);
            $scope.processInCreation.files = newFiles.concat($scope.processInCreation.files);
        }

        // Validate no duplicated files and folders for concat
        function addDocs(newDocs, docsActual) {
            if (docsActual && docsActual.length) {
                for (var i=0; i < docsActual.length; i++ ) {
                    var docActual = docsActual[i];
                    newDocs = newDocs.filter(function(doc){
                        return  docActual.id != doc.id;
                    });
                }
            }
            return newDocs;
        };

        // Remove files and folders from arrays list one by one
        $scope.docsSustractObject = function(docToRemove) {
            // validate if previsuly exists docs selected
            var type = docToRemove.type == 'folder' ? 'folders' : 'files';
            var docsActual = $scope.processInCreation[type] || [];

            // Remove by equality id
            docsActual = docsActual.filter(function(doc) {
                return doc.id != docToRemove.id;
            });

            // Rewrite is needed
            $scope.processInCreation[type] = docsActual;
        };
        
        // Move into slides 
        $scope.goSlide = function(slide) {
            // Define actual slide visible
            $scope.activeSlide = $scope.activeSlide || 0;

            // validate if next or previus
            if (slide == 'next' || slide == undefined) {
                if ( $scope.activeSlide < $scope.TOTALSLIDES ) {
                    $scope.activeSlide++;
                } else {
                    $scope.activeSlide = 0;
                }
            } else if (slide == 'prev') {
                if ( $scope.activeSlide > 0 ) {
                    $scope.activeSlide--;
                } else {
                    $scope.activeSlide = $scope.TOTALSLIDES;
                }
            } else {
                $scope.activeSlide = slide;
            }
        };
        /*
         * StringError: string divide with ---- and concat error with &&&&
         */
        function convertDetailsLogsToObject( stringError ) {
            var newArrayDetails = angular.copy(stringError || '');

            // Validation
            if ( !newArrayDetails || !typeof(newArrayDetails).match(/string/i) ) {
                return newArrayDetails;
            }

            // split items
            newArrayDetails = newArrayDetails.split('----');

            // each object is one folder or file
            for (var i=0; i < newArrayDetails.length; i++ ) {
                // Divide name and error
                var arrayFile = newArrayDetails[i].split('&&&&');

                // replace string to object
                newArrayDetails[i] = {
                    name: arrayFile[0],
                    error: arrayFile[1]
                };
            }

            return newArrayDetails;
        };

    }]);
})();

