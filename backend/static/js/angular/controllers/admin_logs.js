(function() {
    "use strict";
    gApp.controller('adminIndex', ['$log', '$scope', '$rootScope', '$window', '$location', '$interval', 'EndpointsService', 'eforModal', 'Notification', '$filter',
    function($log, $scope, $rootScope, $window, $location, $interval, endpointsService, $efmodal, notification, $filter) {

        $rootScope.DOMAIN = $window.DOMAIN;
        $rootScope.USER = $window.USER_EMAIL;
        $scope.STATUS = $window.PROCESSES_STATUS;
        $scope.PROCESSES_TYPE = $window.PROCESSES_TYPE;
        $scope.pagesTokens = {}; // save tokens id
        $scope.logsList; // [] Contain list process
        // Task dates from constants
        $scope.dateDaily = $window.SYNC_TASK_DAILY;
        $scope.dateWeekely = $window.SYNC_TASK_WEEKELY;
        $scope.dateMonthly = $window.SYNC_TASK_MONTHLY;
        var translate = $filter('translate');

        
        // Initialize with authorization
        $scope.$on(endpointsService.ENDPOINTS_READY, function() {
            endpointsService.authorize(CLIENT_ID, SCOPES, initialize);
        });

        function initialize() {
            // Create empty list container processes
            $scope.logsList = [];
            // List
            $scope.listLogs();
        };

        $scope.listLogs = function(nextPage) {
            var data = {
                domain : $window.DOMAIN,
                order : '-created',
                limit : '20'
            };
             
            // Validate token page
            if (nextPage) {
                data.pageToken = nextPage;
            }

            endpointsService.listLogs(data, function(response) {
                // Validate response
                if (response.error || response.code >= 400 ) {
                    // Error authorization
                    if (response.error.code == 503) {
                        $log.error(translate('notification_request_authorization'), response );
                        notification.error( translate('notification_request_authorization') );
                        // Recall endpoint with autorization code
                        var reCallEndpoint = function(){ return $scope.listLogs(nextPage) };
                        endpointsService.authorize(CLIENT_ID, SCOPES, reCallEndpoint);
                        return false;
                    }
                    
                    // Other error
                    $log.error( translate('notification_error_on_list_logs'), response );
                    notification.error( translate('notification_error_on_list_logs') );
                    return false;
                }

                // Save nextoken for pagination
                $scope.pagesTokens.cursor = response.nextPageToken || null;

                // Apply respoonse to view model
                $scope.logsList = response.items || [];

            });
        };


        // TODO: Join width endpoints
        $scope.openDetails = function(process) {

                $scope.processInView = process;
                $scope.processInView.total_processed = 0;
                // Assign name
                $scope.processInView.name = $scope.processInView.name || translate($window.PROCESSES_TYPE[$scope.processInView.type]);


               if($scope.processInView.details){
                    try{
                        $scope.processInView.proccessDetail = JSON.parse($scope.processInView.details);
                        $scope.processInView.total_processed = $scope.processInView.proccessDetail.length;
                    } catch(error){
                        $scope.processInView.proccessDetailPlane = $scope.processInView.details;
                    }
               }

            if(process.type == 2){
                $efmodal.open({templateUrl:'/static/html/modals/details_process_permissions.html', scope:$scope});

            } else {
                $efmodal.open({templateUrl:'/static/html/modals/details_process_orphans.html', scope:$scope});

            };
        }

        // Pagination controller
        $scope.goToPage = function(contrl) {
            /* Pagination select from pane active*/
            var paginationUse = $scope.pagesTokens;
            /* obtain controller 'next'/'back'/'home'*/
            var pageToken;

            switch (contrl) {
                case "home":
                    paginationUse.cursorBack = [];
                    paginationUse.cursorCurrent = null;
                    break;

                case "next":
                    if (paginationUse.cursorCurrent != null) {
                        paginationUse.cursorBack = paginationUse.cursorBack || [];
                        paginationUse.cursorBack.push(paginationUse.cursorCurrent);
                    }

                    paginationUse.cursorCurrent = paginationUse.cursor;
                    pageToken = paginationUse.cursor;
                    break;

                case 'current':
                    pageToken = paginationUse.cursorCurrent;
                    break;

                case "back":
                    paginationUse.cursorBack = paginationUse.cursorBack || [];
                    paginationUse.cursorCurrent = paginationUse.cursorBack.pop();
                    pageToken = paginationUse.cursorCurrent;
                    break;
            }

            $scope.listLogs(pageToken);
        };

        /*
         * StringError: string divide with ---- and concat error with &&&&
         */
        function convertDetailsLogsToObject( stringError ) {
            var newArrayDetails = angular.copy(stringError || '');

            // Validation
            if ( !newArrayDetails || !typeof(newArrayDetails).match(/string/i) ) {
                return newArrayDetails;
            }

            // split items
            newArrayDetails = newArrayDetails.split('----');

            // each object is one folder or file
            for (var i=0; i < newArrayDetails.length; i++ ) {
                // Divide name and error
                var arrayFile = newArrayDetails[i].split('&&&&');

                // replace string to object
                newArrayDetails[i] = {
                    name: arrayFile[0],
                    error: arrayFile[1]
                };
            }

            return newArrayDetails;
        };



    }]);
})();

