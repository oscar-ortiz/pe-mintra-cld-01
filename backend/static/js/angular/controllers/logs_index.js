(function() {
    "use strict";
    
        // DEMO info
        window._responseModel = [{
            id: 123456,
            type: 2,
            created: '2016-08-05T13:23:03.063480',
            name : '',
            status: 1,
            owner: ['owrner1@own.own', 'owrner2@own.own'],
            new_owner: '',
            folders: [{name:'f1'},{name:'f2'},{name:'f3'}],
            files: [{name:'c1'},{name:'c2'},{name:'c3'}],
            frequency: 1, // 0 is none
            writers: ['oscar@ll.ll', 'asd@asd@asd.asd'], // {user_email:'edgar@eforcers.com', type:'user'},
            viewers: ['writter1@wr.wr', 'writter2@wr.wr'],
            commenters: ['vw@vw.vw', 'vw2324@vw.vw'],
            none: ['none1@nn.nn','none2@nn.nn'], // without any permission
            reset_permissions: true,
            writer_canshare: true,
            viewer_cancopy: true,
        }];

    gApp.controller('adminIndex', ['$log', '$scope', '$rootScope', '$window', '$location', '$interval', 'EndpointsService', 'DriveService','eforModal', 'Notification', '$filter',
    function($log, $scope, $rootScope, $window, $location, $interval, endpointsService, DriveService, $efmodal, notification, $filter) {
    console.log(_responseModel);

        $rootScope.DOMAIN = $window.DOMAIN;
        $rootScope.USER = $window.USER_EMAIL;
        $scope.processesList; // [] Contain list process
        $scope.STATUS = $window.PROCESSES_STATUS;
        $scope.PROCESSES_TYPE = $window.PROCESSES_TYPE;
        $scope.processInCreation; // it is used in the modal
        $scope.activeSlide; // Save slide visibe
        $scope.pagesTokens = {}; // save tokens id
        // Task dates from constants
        $scope.dateDaily = $window.SYNC_TASK_DAILY;
        $scope.dateWeekely = $window.SYNC_TASK_WEEKELY;
        $scope.dateMonthly = $window.SYNC_TASK_MONTHLY;
        $scope.TODAY = new Date().toISOString();
        $scope.processInView = undefined; // {}
        var translate = $filter('translate');
        var USER_TYPE = ['anyone', 'user', 'group', 'domain'];
        var PERMISSIONS = ['owner', 'writers', 'commenters', 'viewers', 'none'];
        // Models empty
        var ModelNewProccess = {
            type: undefined,
            name : '',
            status: undefined,
            owner: [],
            new_owner: '',
            folders: [],
            files: [],
            frequency: 0, // 0 is none
            other_permissions: [], // {user_email:'edgar@eforcers.com', type:'user'},
            commenters: [],
            viewers: [],
            none: [], // without any permission
            reset_permissions: false,
            viewer_cancopy: true,
            writer_canshare: true,
            esp_permissions: [],
            // below properties is only used in view
            searchAs: 1, // type user
            // template for add user permission
            userOrGroupToAdd: {
                user_email: '',
                type: '',
                permission: ''
            },
            actionsGroupsView: {
                'group': [],
                'user': [],
                'domain': []
            },
            showFrequency: false,
            tokenUser: '',
            userQuery: '',
            usersResult: [],
            userOwnerQuery: '',
            usersOwnerResult: [],
            userPermissionQuery: '',
            usersPermissionsResult: []
        };

        // Initialize with authorization
        $scope.$on(endpointsService.ENDPOINTS_READY, function() {
            endpointsService.authorize(CLIENT_ID, SCOPES, initialize);
        });

        function initialize() {
            // Create empty list container processes
            $scope.processesList = [];

            // Simulation response
            var _responseModel = $window._responseModel || undefined;
            if (_responseModel && _responseModel.length) {
                $scope.processesList = _responseModel;
                $scope.$apply();
            }

        };

        // Get list process
        // TODO: Bind with process endpoints
        $scope.listProcess = function(nextpage) {
            var data = {
                domain : $window.DOMAIN,
                order : '-timestamp',
                limit : '30'
            };
             
            // Validate token page
            if (nextPage) {
                data.pageToken = nextPage;
            }
            endpointsService.lisProcess(data, function(response) {});

        };

        // Open modal process owner change
        $scope.openCreateProcess = function(type) {
            // Empty model
            $scope.processInCreation = angular.copy(ModelNewProccess);
            $scope.processInCreation.type = type;

            // Watch var for manage permisionns switch
            $scope.$watch('processInCreation.searchAs', function(newValue, oldValue) {
                // Reset and clear query selected
                $scope.clearUserPermissionSelected();
                var input = $('input[ng-model="scope.processInCreation.userPermissionQuery"]');
                
                // Is domain
                if (newValue == 3) {
                    $scope.setUsersPermissions($rootScope.DOMAIN);
                    disableInput(input, true);
                } else {
                    disableInput(input, false);
                }
            });

            // Open modal type
            if ( type == 1 ) {
                $efmodal.open({templateUrl:'/static/html/modals/change_owner.html', scope:$scope});
            } else if ( type == 2 ) {
                $efmodal.open({templateUrl:'/static/html/modals/manager_permissions.html', scope:$scope});
            } else if ( type == 3 ) {
                $efmodal.open({templateUrl:'/static/html/modals/search_orphans.html', scope:$scope});
            }
        };
        
        // TODO: Join width endpoints
        $scope.openDetails = function(process) {
            $scope.processInView = process;
            // Assign name
            $scope.processInView.name = $scope.processInView.name || translate($window.PROCESSES_TYPE[$scope.processInView.type]);

            // Open modal width template
            $efmodal.open({templateUrl:'/static/html/modals/details_process.html', scope:$scope});
        };

        // Redirect find user or group
        $scope.getUsersGroupByFind = function(){
            // Search in users
            var query = $scope.processInCreation.userPermissionQuery;
            
            // data for endpoints
            var data = {
                domain: $rootScope.DOMAIN,
                prefix: query
            };

            // Find user|groups|domain {1:users, 2:groups, 3:domain}
            if ( $scope.processInCreation.searchAs == 1 ) {
                   // Connect to endpoint
                endpointsService.listDriveUsers(data, function(response) {
                    // Validate response
                    if (response.error || response.code >= 400 ) {
                        // Error authorization
                        if (response.error.code == 503) {
                            $log.error(translate('notification_request_authorization'), response );
                            notification.error( translate('notification_request_authorization') );
                            // Recall endpoint with autorization code
                            endpointsService.authorize(CLIENT_ID, SCOPES, $scope.getUsersByFind);
                            return false;
                        }
                        
                        // Other error
                        $log.error(translate('notification_error_on_search_users'), response );
                        notification.error( translate('notification_error_on_search_users') );
                        return false;
                    }

                    // Validate resul = 0 or undefined
                    if (response.items < 1 || !response.items) {
                        $log.info(translate('notification_users_not_found'), $scope.processInCreation.userQuery );
                        notification.info( translate('notification_users_not_found') );
                        return false;
                    }
                    
                    // Write response items
                    $scope.processInCreation.usersPermissionsResult = response.items;
                    
                });

            } else if ( $scope.processInCreation.searchAs == 2 ) {
                // Connect to endpoint
                endpointsService.listDriveGroups(data, function(response) {
                    // Validate response
                    if (response.error || response.code >= 400 ) {
                        // Error authorization
                        if (response.error.code == 503) {
                            $log.error(translate('notification_request_authorization'), response );
                            notification.error( translate('notification_request_authorization') );
                            // Recall endpoint with autorization code
                            endpointsService.authorize(CLIENT_ID, SCOPES, $scope.getUsersByFind);
                            return false;
                        }
                        
                        // Other error
                        $log.error(translate('notification_error_on_search_groups'), response );
                        notification.error( translate('notification_error_on_search_groups') );
                        return false;
                    }

                    // Validate resul = 0 or undefined
                    if (response.items < 1 || !response.items) {
                        $log.info(translate('notification_groups_not_found'), $scope.processInCreation.userQuery );
                        notification.info( translate('notification_groups_not_found') );
                        return false;
                    }
                    
                    // Write response items
                    $scope.processInCreation.usersPermissionsResult = response.items;
                                        
                });
            
            // ommited for domain selected
            } else if ( $scope.processInCreation.searchAs != 3 ) {
                notification.warning(translate('notification_not_user_type_selected'));
            }
        };

        // Create process for Owbership change
        $scope.getUsersByFind = function(modelToShow) {
            // Select scope for slide 0 or 1
            var query = $scope.activeSlide == 0 ? $scope.processInCreation.userQuery : $scope.processInCreation.userOwnerQuery;
            // data for endpoints
            var data = {
                domain: $rootScope.DOMAIN,
                prefix: query
            };

            // Connect to endpoint
            endpointsService.listDriveUsers(data, function(response) {
                // Validate response
                if (response.error || response.code >= 400 ) {
                    // Error authorization
                    if (response.error.code == 503) {
                        $log.error(translate('notification_request_authorization'), response );
                        notification.error( translate('notification_request_authorization') );
                        // Recall endpoint with autorization code
                        endpointsService.authorize(CLIENT_ID, SCOPES, $scope.getUsersByFind);
                        return false;
                    }
                    
                    // Other error
                    $log.error(translate('notification_error_on_search_users'), response );
                    notification.error( translate('notification_error_on_search_users') );
                    return false;
                }

                // Validate resul = 0 or undefined
                if (response.items < 1 || !response.items) {
                    $log.info(translate('notification_users_not_found'), $scope.processInCreation.userQuery );
                    notification.info( translate('notification_users_not_found') );
                    return false;
                }
                
                // Filter slide section 
                if ( $scope.activeSlide == 0  ) {
                    // Response succesfull
                    $scope.processInCreation.usersResult = response.items;
                } else if ( $scope.activeSlide == 1 ) {
                    // Response to user
                    $scope.processInCreation.usersOwnerResult = response.items;
                }
            });
        };

        // Validate all form
        $scope.validateOwnerShipChange = function() {
            // Validation for assign permissions
            if ( $scope.activeSlide == 0  && $scope.processInCreation.type == 2 ) {
                // Validation files selected
                if (!$scope.processInCreation.folders.length && !$scope.processInCreation.files.length ) {
                    notification.error(translate('notification_no_docs_selected'));
                    return false;
                }
                // Go next slide
                $scope.goSlide('next');

            // Validation for orphans files
            } else if ( $scope.processInCreation.type == 3 && $scope.activeSlide == 0 ) {
                // Validation if users is selecteds
                if ( !$scope.processInCreation.owner || $scope.processInCreation.owner.length == 0 ) {
                    notification.error(translate('notification_not_users_selected'));
                    return false;
                }

            } else if ( $scope.processInCreation.type == 2 && 1 && $scope.activeSlide == 1 ) {
                    var valid = $scope.processInCreation.actionsGroupsView.user.length;
                    valid = valid || $scope.processInCreation.actionsGroupsView.group.length;
                    valid = valid || $scope.processInCreation.actionsGroupsView.domain.length;
                    valid = valid || $scope.processInCreation.new_owner;

                    if (!valid) {
                        notification.error(translate('create_no_permissions_assigned'));
                        return false;
                    }

                // go next slide
                $scope.goSlide('next');

            } else if ( $scope.activeSlide == 2 ) {
                // Validate if showFrequency is selected
                if ($scope.processInCreation.showFrequency) {

                    // Validate folders select
                    if (!$scope.processInCreation.folders.length) {
                        notification.error(translate('notification_no_folder_selected'));
                        return false;
                    }

                    // Validate name for process
                    $scope.processInCreation.name = $filter('validName')($scope.processInCreation.name);
                    if (!$scope.processInCreation.name) {
                        notification.error(translate('notification_no_name_process_selected'));
                        return false;
                    }

                    // Validate frecuence selected
                    if (!$scope.processInCreation.frequency) {
                        notification.error(translate('notification_no_period_selected'));
                        return false;
                    }
                }
            }
            // Success
            return true;

        };

        // Clear user selected for GoogleDrive
        $scope.clearUserSelected = function() {
            $scope.processInCreation.userQuery = '';
            $scope.processInCreation.owner = [];
            $scope.processInCreation.tokenUser = '';
            $scope.processInCreation.files = [];
            $scope.processInCreation.folders = [];
            // Enable inputSearch
            var input = $('input[ng-model="scope.processInCreation.userQuery"]');
            disableInput(input, false);
        };

        // Select new owner for files selected
        $scope.setUserOwner = function(user) {
            // Response successfull
            $scope.processInCreation.new_owner = user;
            $scope.processInCreation.userOwnerQuery = user;
            // Disable inputSearch
            var input = $('input[ng-model="scope.processInCreation.userOwnerQuery"]');
            disableInput(input,true);
        };
        
        // Select new owner for files selected
        $scope.setUserForOrphansFiles = function(newUser) {
            // Validate exists
            $scope.processInCreation.owner = $scope.processInCreation.owner || [];

            // Prevent Exists user in group
            if ( $scope.processInCreation.owner.indexOf(newUser) > -1 ) {
                notification.info(translate('notification_user_already_exists'));
                // Clean resuslt
                $scope.processInCreation.usersResult = [];

                return false;
            }

            $scope.processInCreation.owner.unshift(newUser);

            // Clear input search
            $scope.clearInputUser();
        };

        // Clear for ouser in orphans files
        $scope.clearInputUser = function() {
            $scope.processInCreation.userQuery = '';
            $scope.processInCreation.usersResult = [];
        };

        // Remove from list
        $scope.userSustractObject = function(removeUser) {

            // Get index in array
            var userIndex = $scope.processInCreation.owner.indexOf(removeUser);

            // Validate exist
            if(userIndex >= 0) {
                $scope.processInCreation.owner.splice(userIndex, 1);
            }
        };

        // Clear user selected permissions user query
        $scope.clearUserOwnerSelected = function() {
            $scope.processInCreation.new_owner = '';
            $scope.processInCreation.userOwnerQuery = '';
            // Enable inputSearch
            var input = $('input[ng-model="scope.processInCreation.userOwnerQuery"]');
            disableInput(input, false);
        };

        // Select user or group for pass to list
        $scope.setUsersPermissions = function(userOrGroup) {
            var isDomain = userOrGroup.match('@') ? false : true;
            
            // Validate if email already exist
            if ( emailExistsInGroups(userOrGroup) ) {
                if (isDomain) {
                    notification.warning(translate('notification_domain_already_add'));
                } else {
                    // filter message notification user or group
                    if ( $scope.processInCreation.searchAs == 2 ) {
                        notification.warning(translate('notification_group_already_exists'));
                    } else {
                        notification.warning(translate('notification_user_already_exists'));
                    }
                }
                return false;
            }


            // Extract type from radio buttons
            var type = $scope.processInCreation.searchAs;
            // Rewrite input query with all user_email
            $scope.processInCreation.userPermissionQuery = userOrGroup;
            // Prevetn error for object
            $scope.processInCreation.userOrGroupToAdd = $scope.processInCreation.userOrGroupToAdd || {};
            // Assign email to user object
            $scope.processInCreation.userOrGroupToAdd.user_email = userOrGroup;
            // Assign type to user object
            $scope.processInCreation.userOrGroupToAdd.type = USER_TYPE[type];
            // Disable inputSearch 
            var input = $('input[ng-model="scope.processInCreation.userPermissionQuery"]');
            disableInput(input, true);

        };
    
        // Add user
        $scope.addUserToManagePermission = function() {
            // Get peermission from user for add
            var userPermission = $scope.processInCreation.userOrGroupToAdd.permission;

            // Validate if permision is selected
            if (!userPermission) {
                notification.info(translate('notification_not_user_selected'));
                $log.log('EDMG: Error on validate user to add:', userToAdd, userPermission);
               return false; 
            }
            
            // Validate if permision is for owner or not
            if (userPermission == 'new_owner') {
                $scope.processInCreation.new_owner = $scope.processInCreation.userOrGroupToAdd.user_email;

            } else {
                // Prepare object to push
                var userToAdd = {
                    type: $scope.processInCreation.userOrGroupToAdd.type,
                    user_email: $scope.processInCreation.userOrGroupToAdd.user_email,
                    permission: userPermission // Used in view
                };

                // Add to view and model to send and validate
                addUserToActionGroup(userToAdd, userPermission);
            }

            // Clear input
            $scope.clearUserPermissionSelected();
        };

        // Validate for add user o group
        function emailExistsInGroups(userEmail) {
            var exist = false;
            var groups = ['writers', 'commenters', 'viewers', 'none'];
            // iteration in all permissions
            main: for ( var i=0; i < groups.length; i++) {
                // get group 
                var group = groups[i];

                // Find user in lists
                for ( var j=0; j < $scope.processInCreation[group].length; j++ ) {
                    // Get user
                    var iterUser = $scope.processInCreation[group][j];

                    // Validation user
                    if (iterUser.user_email == userEmail) {
                        exist = true;
                        break main;
                    }
                }
            }


            if ( userEmail == $scope.processInCreation.new_owner) {
                exist = true;
            }

            // Not exist
            return exist;
        };

        // Add to Actiongroup
        function addUserToActionGroup(userToAdd, userPermission) {
            // Validate user selected
            if (!userToAdd || !userToAdd.user_email) {
                notification.info(translate('notification_not_user_selected'));
                $log.log('EDMG: Error on validate user to add:', userToAdd, userPermission);
               return false; 
            }

            // Validate permission selected
            if (!userPermission) {
                notification.info(translate('notification_not_user_permission_selected'));
                $log.log('EDMG: on validate permission to add:', userToAdd, userPermission);
                return false; 
            }

            // Add to group actions
            if (!$scope.processInCreation[userPermission]) {
                $log.warn(translate('type permission not exist:', userPermission));
                return false;
            }

            // Add user to permisson group Model
            $scope.processInCreation[userPermission].push(userToAdd);

            // Add to view user|group|domain
            if (userToAdd.type) {
                $scope.processInCreation.actionsGroupsView[userToAdd.type].push(userToAdd);

                if (userToAdd.type == 'domain') {
                    $scope.processInCreation.searchAs = 2;
                    $scope.processInCreation.searchAs = 3;
                }
            }


        };

        // Remove to ActionGroup
        $scope.removeUserToActionGroup = function(removeUser) {
            var removeUserCopy = angular.copy(removeUser);

            // Validate if removeUser is Owner (string)
            console.log( typeof(removeUserCopy) );

            if ( (typeof(removeUserCopy)).match(/string/i) ) {
                // Empty user_owner
                $scope.processInCreation.new_owner = '';
            
            } else {
                // Get permissions
                var userPermission = removeUserCopy.permission;
                
                // Remove from model to send
                $scope.processInCreation[userPermission] = $scope.processInCreation[userPermission]
                    .filter( function(element) {
                        return element.user_email == removeUserCopy.user_email ? false : true;
                    });
            
                // Remove from view
                $scope.processInCreation.actionsGroupsView[removeUserCopy.type] = $scope.processInCreation.actionsGroupsView[removeUserCopy.type]
                    .filter( function(element) {
                        return element.user_email == removeUserCopy.user_email ? false : true;
                    });
            }

        };

        // Update user permission
        $scope.updateUserPermissions = function(userToUpdate, newPermission) {
            // Get permissions
            var oldPermission = userToUpdate.permission;
            
            // update permission in view groups
            userToUpdate.permission = newPermission;
        
            // Find and update permission
            for (var i=0; i <= $scope.processInCreation[oldPermission].length; i++) {
                
                // Get user iteration
                var userInModel = $scope.processInCreation[oldPermission][i];
                // Validate equality and update permission
                if (userToUpdate.user_email == userInModel.user_email) {
                    // Update permission in view groups
                    userInModel.permission = newPermission;
                    // Remove from old permission group
                    $scope.processInCreation[oldPermission].splice(i,1);
                    // pasete user in new permission group
                    $scope.processInCreation[newPermission].push(userInModel);
                    break;
                }
            }

        };

        // Clear user selected owner
        $scope.clearUserPermissionSelected = function() {
            $scope.processInCreation.userPermissionQuery = '';
            // Clear from copy model
            $scope.processInCreation.userOrGroupToAdd =angular.copy( ModelNewProccess.userOrGroupToAdd);
            // Clear searched result
            $scope.processInCreation.usersPermissionsResult = [];
            // Enable inputSearch
            var input = $('input[ng-model="scope.processInCreation.userPermissionQuery"]');
            disableInput(input, false);
        };

        // Set as as user selected and use token from backend
        $scope.getTokenUser = function(user) {
            var data = {
                domain: $rootScope.DOMAIN,
                emailUser: user 
            };

            endpointsService.obtainAccessToken(data, function(response) {
                // Validate response
                if (response.error || response.code >= 400 ) {
                    // Error authorization
                    if (response.error.code == 503) {
                        $log.error(translate('notification_request_authorization'), response );
                        notification.error( translate('notification_request_authorization') );

                        // Recall endpoint with autorization code
                        var reCallEndpoint = function(){ return $scope.getTokenUser(user) };
                        endpointsService.authorize(CLIENT_ID, SCOPES, reCallEndpoint);
                        return false;
                    }
                    
                    // Other error
                    $log.error(translate('notification_error_on_ask_token'), response );
                    notification.error( translate('notification_error_on_ask_token') + ': ' + user );
                    return false;
                }

                // Validate resul = 0 or undefined
                if (response.items < 1 || !response.items) {
                    $log.info(translate('notification_not_request_token'), $scope.processInCreation.userQuery );
                    notification.info( translate('notification_not_request_token') );
                    return false;
                }

                // Response successfull
                $log.info('Token ' + user + ':', response);
                $scope.processInCreation.owner[0] = user;
                $scope.processInCreation.userQuery = user;
                // Is token that uses Drive picker 
                $scope.processInCreation.tokenUser = response.items;
                $scope.processInCreation.usersResult = [];

                // Disable inputSearch
                var input = $('input[ng-model="scope.processInCreation.userQuery"]');
                disableInput(input, true);
            });
        };

        // Send all info to endpoint for create process
        $scope.createProcessOwnner = function() {
            if ( $scope.validateOwnerShipChange() ) {
                var data = angular.copy( $scope.processInCreation );

                // Change status for prepare backend index
                data.status = 1;

                // Frequency
                data.frequency = data.showFrequency ? data.frequency : 0;
                
                // Name
                data.name = data.showFrequency ? data.name : '' ;

                // Send to backend
                endpointsService.saveProcessCreated(data, function(response) {
                    // Validate response
                    if (response.error || response.code >= 400 ) {
                        // Error authorization
                        if (response.error.code == 503) {
                            $log.error(translate('notification_error_on_save'), response );
                            notification.error( translate('notification_error_on_save') );
                            // Recall endpoint with autorization code
                            endpointsService.authorize(CLIENT_ID, SCOPES, $scope.getUsersByFind);
                            return false;
                        }
                        // Other error
                        $log.error(translate('notification_error_on_ask_token'), response );
                        notification.error( translate('notification_error_on_ask_token'));
                        return false;
                    }

                    $log.info('Finish save:', response)
                    // TODO: sync with endpoints
                    $scope.processesList.push( response );

                    $efmodal.close();

                });
            }
        };

        // assign permission to user
        $scope.selectPermission = function(type, user) {
            if (user) {
                // move to another group
                $scope.processInCreation.userOrGroupToAdd.permission = PERMISSIONS[type];
            } else {
                $scope.processInCreation.userOrGroupToAdd.permission = PERMISSIONS[type];
            }
        };

        // TODO: Join with endpoints (COPY FROM ESMG)
        $scope.goToPage = function(contrl) {
            /* Pagination select from pane active*/
            var paginationUse = $scope.pagesTokens;
            /* obtain controller 'next'/'back'/'home'*/
            var pageToken;

            switch (contrl) {
                case "home":
                    paginationUse.cursorBack = [];
                    paginationUse.cursorCurrent = null;
                    break;

                case "next":
                    if (paginationUse.cursorCurrent != null) {
                        paginationUse.cursorBack = paginationUse.cursorBack || [];
                        paginationUse.cursorBack.push(paginationUse.cursorCurrent);
                    }

                    paginationUse.cursorCurrent = paginationUse.cursor;
                    pageToken = paginationUse.cursor;
                    break;

                case "back":
                    paginationUse.cursorBack = paginationUse.cursorBack || [];
                    paginationUse.cursorCurrent = paginationUse.cursorBack.pop();
                    pageToken = paginationUse.cursorCurrent;
                    break;
            }

            $scope.listProcess(pageToken);
        };
        // UTILS
        function disableInput(input, action) {
            if (action) {
                input.attr('disabled', 'disabled');
            } else {
                input.removeAttr('disabled');
            }
        }

        // Open Picker GoogleDrive
        $scope.openDrive = function() {
            DriveService.createPicker({
                    oauthToken: $scope.processInCreation.tokenUser,
                    language: $rootScope.setLanguage
                },
                function(data) {
                    $log.info('files selected: ', data);
                    var docs = data.docs;
                    if (docs && docs.length) {
                        splitDocs(docs);
                    }
            } );
        };

        // Divide foders and files from GoogleDrive respons
        function splitDocs(newDocs) {
            // validate if previsuly exists docs selected
            var foldersActual = $scope.processInCreation.folders || [];
            var filesActual = $scope.processInCreation.files || [];
            var newFolders = [];
            var newFiles = [];

            // Split folders and files
            for (var i=0; i  < newDocs.length; i++) {
                var doc = newDocs[i];
                // add icon for save in endpoint
                doc.icon = doc.iconUrl;
                doc.parent_id = doc.parentId;
                if (doc.type == 'folder'){
                    newFolders.push(doc);
                } else {
                    newFiles.push(doc);
                }
            }

            // Filter if exists or is repeat
            newFolders = addDocs(newFolders, foldersActual);
            newFiles = addDocs(newFiles, filesActual);

            $scope.processInCreation.folders = newFolders.concat($scope.processInCreation.folders);
            $scope.processInCreation.files = newFiles.concat($scope.processInCreation.files);
        }

        // Validate no duplicated files and folders for concat
        function addDocs(newDocs, docsActual) {
            if (docsActual && docsActual.length) {
                for (var i=0; i < docsActual.length; i++ ) {
                    var docActual = docsActual[i];
                    newDocs = newDocs.filter(function(doc){
                        return  docActual.id != doc.id;
                    });
                }
            }
            return newDocs;
        };

        // Remove files and folders from arrays list one by one
        $scope.docsSustractObject = function(docToRemove) {
            // validate if previsuly exists docs selected
            var type = docToRemove.type == 'folder' ? 'folders' : 'files';
            var docsActual = $scope.processInCreation[type] || [];

            // Remove by equality id
            docsActual = docsActual.filter(function(doc) {
                return doc.id != docToRemove.id;
            });

            // Rewrite is needed
            $scope.processInCreation[type] = docsActual;
        };
        
        // Move into slides 
        $scope.goSlide = function(slide) {
            // Define actual slide visible
            $scope.activeSlide = $scope.activeSlide || 0;

            // validate if next or previus
            if (slide == 'next' || slide == undefined) {
                if ( $scope.activeSlide < $scope.TOTALSLIDES ) {
                    $scope.activeSlide++;
                } else {
                    $scope.activeSlide = 0;
                }
            } else if (slide == 'prev') {
                if ( $scope.activeSlide > 0 ) {
                    $scope.activeSlide--;
                } else {
                    $scope.activeSlide = $scope.TOTALSLIDES;
                }
            } else {
                $scope.activeSlide = slide;
            }
        };


    }]);
})();

