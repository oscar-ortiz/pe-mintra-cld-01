(function() {
    "use strict";

    var gTask = angular.module('settings', ['ngAnimate', 'mainMenu', '$eforModal', '$loadAppWidget', 'ui-notification']);

    gTask.service('EndpointsService', ['$log', '$q', '$rootScope', '$http', '$window', 'requestNotificationChannel', EndpointsService]);
    gTask.config(configure);
    gTask.config(notificationConfig);
    gTask.controller('configureController', ['$log', '$rootScope', '$window', '$http', '$scope', '$location', 'EndpointsService',
        'eforModal', '$timeout', '$interval', 'Notification', '$translate', '$filter', configureController]);

    // controller
    function configureController($log, $rootScope, $window, $http, $scope, $location, endpointsService, $efmodal, $timeout,
        $interval, notification, $translate, $filter) {

        $scope.loaded = false; //base
        $rootScope.DOMAIN = window.DOMAIN;
        var translate = $filter('translate');
        $scope.search_criteria = '';
        $scope.last_sync_date = '';
        $scope.user_results = [];
        $scope.search_criteria = '';
        $scope.admin_users = []; // contain administrators
        $rootScope.SYNC_ORGS_FINISH = $rootScope.SYNC_ORGS_FINISH || 'SYNC_ORGS_FINISH'; // From admin.js
        //$scope.languageList = $window.translateConfig()[1].map(function(e){ return {'id': e, 'lang':translate(e) } }); // [{},{}] from configure.js
        $scope.languageList = $window.translateConfig()[1].map(function(e){ return {'id': e, 'lang':translate(e) } });

        // Search admin
        $scope.search_criteria = ''; // Query search
        $scope.user_results = []; // Used in search
        // Config
        $scope.configure = {
            api_authorize :  {
                date : null,
                status : 'done' // done_all | sync | done
            },
            sync_users : {
                date : null,
                status : 'done'
            },
            sync_orgs : {
                date : null,
                status : 'done'
            },
            // Extract value language from root selected and conver in object
            language : $scope.languageList.filter(function(e) {
                    return e.id == $rootScope.setLanguage;
                })[0],
            email_notification : false,
            domain : ''
        };

        // When modal is loaded
        // getClientSettings();
        listAdminUsers();
        getClientSettings();

        // Admin functions
        // Connect to endpoint
        $scope.userMatches = function () {
            // Search in users
            var query = $scope.search_criteria;
            
            // data for endpoints
            var data = {
                domain: $rootScope.DOMAIN,
                prefix: query
            };

            endpointsService.listDriveUsers(data, function(response) {
                // Validate response
                if (response.error || response.code >= 400 ) {
                    // Error authorization
                    if (response.error.code == 503) {
                        $log.error(translate('notification_request_authorization'), response );
                        notification.error( translate('notification_request_authorization') );
                        // Recall endpoint with autorization code
                        endpointsService.authorize(CLIENT_ID, SCOPES, $scope.getUsersByFind);
                        return false;
                    }
                    
                    // Other error
                    $log.error(translate('notification_error_on_search_users'), response );
                    notification.error( translate('notification_error_on_search_users') );
                    return false;
                }

                // Validate resul = 0 or undefined
                if (response.items < 1 || !response.items) {
                    $log.info(translate('notification_users_not_found'),  $scope.user_results );
                    notification.info( translate('notification_users_not_found') );
                    return false;
                }
                
                // Write response items
                $scope.user_results = response.items;
                
            });
        };

        function listAdminUsers() {
            var data = {
                domain: $rootScope.DOMAIN
            };

            endpointsService.listAdministrators(data,
                function(response) {

                    if (response.error) {
                        notification.error(translate('error_consulting_admins'));
                        $log.error(translate('error_consulting_admins'), response);
                    } else {
                        $scope.admin_users = response.items;

                    }
                }
            );
        }

        function getClientSettings(){
            var data = { domain: $rootScope.DOMAIN };

            endpointsService.getClientSettings(data,
                function(response){
                    if(response.error){
                        notification.error(response.error.message);
                        $log.error(response.error.message);
                    } else {
                        if (response.last_sync_date){
                            $scope.sync_status = 'done_all';

                            // Extract date format to compare
                            var lastUsersSync = $window.extractDate(response.last_sync_date); // extract AAAA-MM-DD
                            // compare dates and Assign icon
                            $scope.configure.sync_status = $window.dateStringIsEqual(lastUsersSync) ? 'done_all' : 'done';

                        };
                        $scope.configure.email_notification = response.email_notification;

                        // assign langguage from endpoint or browser
                        response.language = response.language ? response.language : $rootScope.changeLanguage()[0];
                        // Select language when page is loaded
                        $rootScope.changeLanguage(response.language);
                    }

                }
            );
        }

        $scope.loadUser = function(email) {
            $log.debug(email);

            var user = {
                domain : $rootScope.DOMAIN,
                email : email,
                is_admin : true
            };

            $scope.setAdministrator(email, true);
            window.reloadMDLDOM($interval);
        };

        // Swich admin true/false and add new
        $scope.setAdministrator = function(user, is_admin) {
            var data = {
                email: user,
                is_admin: is_admin,
                domain: $rootScope.DOMAIN
            };

            endpointsService.assignAdmin(data, function(response) {
                    // Validate response
                    if (response.error || response.code >= 400 ) {
                        // Error authorization
                        if (response.error.code == 503) {
                            $log.error(translate('notification_request_authorization'), response );
                            notification.error( translate('notification_request_authorization') );
                            // Recall endpoint with authorization code
                            var reCallEndpoint = function(){ return $scope.setAdministrator(user, is_admin) };
                            endpointsService.authorize(CLIENT_ID, SCOPES, reCallEndpoint);
                            return false;
                        } else if (response.error.message == 'THE USER DOESNT EXIST'){
                            notification.error(translate('error_configure_admin'));
                            $log.error(translate('error_configure_admin'), response.error);
                        } else if (response.error.message == 'USER IS THE INSTALLER'){
                            notification.error(translate('error_with_installer'));
                            $log.error(translate('error_with_installer'), response.error);
                        }
                        return false;
                    }

                    var userRecived = response;

                    // Search and remove admin from result list
                    angular.forEach( $scope.user_results, function( userResult, key) {
                        // Compare email with user clicked/selected in search result list
                        if ( userResult == user ) {
                            $scope.user_results.splice(key, 1);
                            return false;
                        }
                    });

                    // Search in current admins list for later replace or push item
                    var existUser;
                    angular.forEach($scope.admin_users, function(userAdmin, key) {
                        if ( userAdmin.email === userRecived.email ) {
                            existUser = userAdmin;
                            return false;
                        }
                    });

                    // Push item/user if is not in list active admins
                    if ( !existUser ) {
                        // Create if not Exists
                        $scope.admin_users = $scope.admin_users || [];
                        $scope.admin_users.push(userRecived);
                    }

                    // Notification
                    if ( existUser ) {
                        // The user previsuly was add to admin active list
                        notification(translate('success_configure_admin_previus_add'));

                    } else if ( userRecived.is_admin ) {
                        notification(translate('success_configure_admin'));

                    } else {
                        notification(translate('success_configure_admin_false'));

                    }

                    window.reloadMDLDOM($interval);

                }
                ,{loadingClass: 'pass'}
            );
        };
        // Sync users
        $scope.syncUsers = function($event) {
            var btn = $event.currentTarget;
            btn.disabled = true;
            // add icon and status
            $scope.configure.sync_users.status = 'sync';
            $http({
                url: '/a/' + $rootScope.DOMAIN + '/sync/users/',
                method: 'GET'
            }).then(
                function(response) {
                    btn.disabled = false;
                    $log.debug('syncUsers', response);

                    if (response.data.match('USERS_SYNCED_SUCCESS')) {
                        // add icon and status
                        $scope.configure.sync_status = 'done_all';
                        notification(translate('success_sync_users'));

                        // Reload signatures in admin list
                        if ( $rootScope.initialLoadMethods ) {
                            $rootScope.initialLoadMethods();
                        }

                    } else {
                        // add icon and status
                        $scope.configure.sync_status = 'check';
                        notification.error(translate('error_sync_users'));
                        $log.error(translate('error_sync_users'), response);
                    }
                },
                function(response) {
                    btn.disabled = false;
                    $scope.sync_status = 'done';
                    notification.error(translate('error_sync_users'));
                    $log.error(translate('error_sync_users'), response);

                }
            )
        };

        // To select language from root
        $scope.selectLanguage = function(lang_option) {
            // set in angular translate
            $rootScope.changeLanguage(lang_option.id);

            var data = {
                language : lang_option.id,
            };

            endpointsService.defineLanguage(data, function(response){
                if (response.error){
                    notification.error(translate('error_defining_language'));
                    $log.error(translate('error_defining_language'), response);
                } else {
                    notification(translate('success_defining_language'));
                    $log.debug(translate('success_defining_language'), response);
                }
            });
        };

        $scope.userMatch = function() {
            console.log ('match');
        };

        // To select key for autorize apis
        $scope.key_code = function (text_key) {

            if(!text_key){
                text_key="";
            }

            if(text_key.length > 0 && text_key.length < 200){
                $scope.autorize_status="warning";
                notification(translate('error_lenght_clientkey'));
                return;
             }

            var data = {
                clientKey : text_key,
            };

            endpointsService.autorizeApis(data, function(response){
                if (response.error){
                    notification.error(translate('error_defining_clientkey'));
                    $log.error(translate('error_defining_clientkey'), response);
                    $scope.autorize_status="warning";
                } else {

                    if(text_key){
                        notification(translate('success_defining_clientkey'));
                        $log.debug(translate('success_defining_clientkey'), response);
                        $scope.autorize_status="done_all";
                        $scope.text_key = ""
                    }
                    else{
                        notification(translate('success_test_clientkey'));
                        $log.debug(translate('success_test_clientkey'), response);
                        $scope.autorize_status="done";
                    }
                }


            });

        };

        //receive_email_notifications
        $scope.receiveNotifications = function() {

            var data = {
                domain: $rootScope.DOMAIN,
                email_notification: $scope.configure.email_notification
            };

            endpointsService.receiveEmailNotification(data, function(response) {

                if (response.error) {
                    notification.error(translate('error_defining_language'));
                    $log.error(translate('error_defining_language'), response);

                } else {
                    notification(translate('success_defining_language'));
                    $log.debug(translate('success_defining_language'), response);
                    window.reloadMDLDOM($interval);

                }
            }
            ,{loadingClass: 'pass'}
            );
        }

    }

})();
