(function() {
    "use strict";

    gApp.directive('processList', function() {
        return {
            restric: 'A',
            templateUrl: '/static/html/partials/process_list.html'
        };
    });
    gApp.directive('processEmpty', function() {
        return {
            restric: 'A',
            templateUrl: '/static/html/partials/process_empty.html'
        };
    })
    gApp.directive('processSelectFolders', function() {
        return {
            restric: 'A',
            templateUrl: '/static/html/partials/process_select_folders.html'
        };
    })
    gApp.directive('processSelectUserPermissions', function() {
        return {
            restric: 'A',
            templateUrl: '/static/html/partials/process_select_user_permissions.html'
        };
    })
    gApp.directive('processSelectFrequency', function() {
        return {
            restric: 'A',
            templateUrl: '/static/html/partials/process_select_frequency.html'
        };
    })
    ;

})();
