(function() {
    "use strict";

    window.errorApp = angular.module('errorApp', [
        'ngRoute',
        'pascalprecht.translate'
    ]);
    // From configure.js
    errorApp.config(configure);
    errorApp.config(configRoute);
    errorApp.config(translateConfig);

    errorApp.controller('errorController', ['$log', '$scope', '$location', '$translate', '$filter',
    function errorController($log, $scope, $location, $translate, $filter) {


    }]);

    errorApp.controller('error403', ['$log', '$scope', '$location', '$translate', '$filter',
    function error403($log, $scope, $location, $translate, $filter) {



    }]);

    errorApp.controller('error404', ['$log', '$scope', '$location', '$translate', '$filter',
    function error404($log, $scope, $location, $translate, $filter) {


    }]);

    errorApp.controller('error500', ['$log', '$scope', '$location', '$translate', '$filter',
    function error500($log, $scope, $location, $translate, $filter) {


    }]);

    function ChangeRoute( $rootScope, $route, $log) {
      return function() {
        $rootScope.$on( '$routeChangeSuccess', function() {
          $log.debug('change route: ', $route.current.loadedTemplateUrl );
        });
      }
    };

    function configRoute($routeProvider, $locationProvider) {
       $routeProvider
        .when('/403', {
          templateUrl: '/static/html/errors/403.html',
          controller: 'error403'
        })
        .when('/404', {
          templateUrl: '/static/html/errors/404.html',
          controller: 'error404'
        })
        .when('/500', {
          templateUrl: '/static/html/errors/500.html',
          controller: 'error500',
        })
        .otherwise({
          redirectTo: '/404'
        });
    };

})();
