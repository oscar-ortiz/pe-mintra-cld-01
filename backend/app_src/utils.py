import os, re, binascii
import logging
from constants import PROYECT


def createHashId():
    string_rand = str(binascii.b2a_hex(os.urandom(10)))
    string_rand = re.sub('[/\'",]', '', string_rand) 
    return string_rand

def get_request_objects(request, method=None):
    response = {'get': None, 'post': None}
    # get
    try:
        response['get'] = request.args
    except Exception as e:
        logging.info('get_request_objects_error get method GET [%s]', e)
    
    # post
    try:
        response['post'] = request.get_json(silent=True)
        if not response['post']:
            raise Exception('post not found in arguments')
    except Exception as e:
        try:
            response['post'] = request.json
        except Exception as e:
            logging.info('get_request_objects_error get method POST [%s]', e)
    
    return response




