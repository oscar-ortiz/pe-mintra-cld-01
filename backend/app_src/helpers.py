import os
from google.cloud import datastore

from .utils import createHashId
from constants import SCOPES,SERVICE_ACCOUNT_FILE ,BUCKET_OUTPUT,PROYECT
from google.oauth2 import service_account
import googleapiclient.discovery
import time
from datetime import datetime
from constants import COLLECTION
from google.cloud import firestore

class FirestoreHelper:
    def __init__(self):

        credentials = service_account.Credentials.from_service_account_file(
            SERVICE_ACCOUNT_FILE)

        self.firestore_client = firestore.Client(project=PROYECT,
                                                 credentials=credentials)

    def save_user(self,dni,date,selected):

        doc_ref = self.firestore_client.collection(COLLECTION).document(dni)
        doc_ref.set({
            u'dni': dni,
            u'date': date,
            u'selected': selected,
        })
        return "OK"

    def get_user_database(self,dni):

        list_approval = []

        approval_ref = self.firestore_client.collection("user_test")

        query_ref = approval_ref.where("dni", "==", dni) \
            #.where("status", "==", REVISION) \
            #.where("type", "==", TYPE_CHANGE_METADATA)

        for result in query_ref.get():
            result._data["id_firestore"] = result.id
            list_approval.append(result._data)

        return list_approval

class StorageHelper:
    def __init__(self,admin):

       credentials = service_account.Credentials.from_service_account_file(
           SERVICE_ACCOUNT_FILE, scopes=SCOPES,subject=admin)

       self.service = googleapiclient.discovery.build('storage', 'v1', credentials=credentials)

    def CopyResult(self,cloudStorageSink,client,user,type,bucket):

        now = datetime.now()

        for object in  cloudStorageSink:

            path = object['objectName'].split("/")

            service_request = self.service.objects().copy(
                sourceBucket=object['bucketName'],sourceObject=object['objectName'], destinationBucket=bucket,destinationObject=client+"/"+user+"/"+type+"/"+now.strftime("%m-%d-%Y, %H:%M:%S")+"/"+path[2],body="")
            response = service_request.execute()

        return "OK"
    

class CloudTaskHelper:
    def __init__(self):
        credentials = service_account.Credentials.from_service_account_file(
            SERVICE_ACCOUNT_FILE, scopes=SCOPES)

        self.service = googleapiclient.discovery.build('cloudtasks', 'v2beta3', credentials=credentials)

    def CreateTask(self,parent,task):

        body = {
            'task': task
        }
        service_request = self.service.projects().locations().queues().tasks().create(parent=parent,body=body)
        response = service_request.execute()
        return response