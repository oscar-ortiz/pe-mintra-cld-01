import logging
from flask import Flask, render_template, Blueprint


view = Blueprint('view', __name__)


@view.route('/')
def root():

    logging.info("Inicio pagina principal")


    return render_template('landing_page.html')


