from flask import Blueprint, jsonify, request
from .helpers import FirestoreHelper
import logging
from constants import PROYECT
import time

task = Blueprint('task', __name__)


#@task.route('/search_user', methods=['GET'])
def get_user_to_database():

    try:

        return "listo"

    except Exception as e:
        logging.info('info_process:_error_en_generar_proceso')
        logging.error(e)


@task.route('/set_user_to_database/<valori>', methods=['GET'])
def set_user_to_database(valori):

    intentos = 0
    i= int(valori)
    limit = i + 1000
    try:

        firestore_client = FirestoreHelper()

        while i != limit:

            try:

                dni = str(i)
                date = "2020-01-13"
                selected = 0

                firestore_client.save_user(dni,date,selected)
                i += 1
                time.sleep(0.010)
            except Exception as e:

                intentos += 1
                logging.info('info_process:_error_en_endpoint_guardar_total_' + str(i)+"_intento_"+str(intentos))

                if intentos >5:
                    return "fallo"
        logging.info('info_process:_finaliza_valor_'+ str(i))

        return "listo"

    except Exception as e:
        logging.info('info_process:_error_en_generar_proceso')
        logging.error(e)



@task.route('/get_user_to_database/<dni>', methods=['GET'])
def get_user_to_database_test(dni):

    try:

        firestore_client = FirestoreHelper()
        user_response = firestore_client.get_user_database(dni)

        if user_response:

            dni = user_response[0]["dni"]
            logging.info('info_process:_leido_' + str(dni))

        else:
            logging.info('info_process:_no_encontrado_' + str(dni))

        #time.sleep(0.010)
        return "listo"

    except Exception as e:
        logging.info('info_process:_error_en_obtener_usuario')
        logging.error(e)