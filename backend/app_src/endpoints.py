from .helpers import FirestoreHelper,StorageHelper,CloudTaskHelper
from flask import Flask, render_template, Blueprint, jsonify, request

from .utils import get_request_objects
from constants import PROYECT
import time
import random

import logging

endpoints = Blueprint('endpoints', __name__)


@endpoints.route('/search_user', methods=['GET'])
def generateTask():

    try:

        i = 0
        guardados = 0
        intentos = 0;
        logging.info('info_process:_inicializa_proceso_guardar_firestore')

        while guardados != 5000:

            while i != 1000:

                try:

                    dni = str(i)
                    date = "2020-01-13"
                    selected = 0

                    firestore_client = FirestoreHelper()
                    firestore_client.save_user(dni, date, selected)
                    i += 1
                    time.sleep(0.001)
                except Exception as e:

                    intentos += 1
                    logging.info(
                        'info_process:_error_en_endpoint_guardar_total_' + str(i) + "_intento_" + str(intentos))

                    if intentos > 5:
                        return "fallo"

            guardados += i
            i = 0
            logging.info('info_process:_finaliza_mil_1000')
        logging.info('info_process:_finaliza_proceso_guardar_firestore')

        return "listo"

    except Exception as e:

        logging.info('info_process:_error_en_endpoint')
        logging.error(e)


@endpoints.route('/save_users_to_database/', methods=['GET'])
def save_client():
    # Construct the request body.

    #Create a client.
    client = CloudTaskHelper()

    valori = 0
    i = 0

    logging.info('info_process:_inicializa_proceso_guardar_firestore')

    project = "edmg-dev-smartfiles-mod"
    location = "us-central1"

    while i != 10:

        task = {
            'app_engine_http_request': {  # Specify the type of request.
                'http_method': 'GET',
                'relative_uri': '/set_user_to_database/' + str(valori)
            }
        }

        queue = 'agend-'+ str(i)
        #queue = 'agend-1'

        # Construct the fully qualified queue name.
        parent = "projects/" + project + "/locations/" + location + "/queues/" + queue

        # Crea Tarea
        response = client.CreateTask(parent, task)

        i += 1
        valori += 1000

    return "ok"



@endpoints.route('/get_users_to_database_test/', methods=['GET'])
def get_client_test():
    # Construct the request body.

    #Create a client.
    client = CloudTaskHelper()

    valori = 0
    i = 0
    queue_number = 0

    logging.info('info_process:_inicializa_proceso_obtener_firestore')

    project = "edmg-dev-smartfiles-mod"
    location = "us-central1"

    while i != 10000:

        queue_number = random.randrange(10)

        task = {
            'app_engine_http_request': {  # Specify the type of request.
                'http_method': 'GET',
                'relative_uri': '/get_user_to_database/' + str(i)
            }
        }

        queue = 'agend-'+ str(queue_number)
        #queue = 'agend-1'

        # Construct the fully qualified queue name.
        parent = "projects/" + project + "/locations/" + location + "/queues/" + queue

        # Crea Tarea
        response = client.CreateTask(parent, task)

        i += 1
        #queue_number += 1

        #if queue_number > 10:
        #    queue_number = 0

    logging.info('info_process:_termina_proceso_obtener_firestore')

    return "ok"