SCOPES = ['https://www.googleapis.com/auth/ediscovery',
          'https://www.googleapis.com/auth/cloud-platform',
          'https://www.googleapis.com/auth/devstorage.full_control',
          'https://www.googleapis.com/auth/devstorage.read_write'
         ]


SERVICE_ACCOUNT_FILE = 'test_services.json'
COLLECTION = "user_test"
PROYECT = "pe-intercorp-gcp-01"


BUCKET_OUTPUT = "pruebasvault001"
