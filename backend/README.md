# Vison API AppEngine Standar Python 3.7



## Installation



### Firebase and GCP
- Create GCP Project ([Console](https://console.cloud.google.com))
- Create Firebase Project ([Console](https://console.firebase.google.com/u/0/?hl=es-419&pli=1) | [Doc](https://firebase.google.com/docs/web/setup?hl=es-419))
    - Start with Firebase ([Doc](https://firebase.google.com/docs/storage/web/start?hl=es-419))

### Require:
- **Python 2.7** for run App Engine local
- **Python 3.7** like python3
- **pip** to install python dependences
- **Browser file API** ((doc)[https://www.html5rocks.com/es/tutorials/file/dndfiles/])



```bash
# install global virtualenv
python3 -m pip install virtualenv;
# create folder enviroment
python3 -m virtualenv virtual_env;
# activate virtualenv in path;
source virtual_env/bin/activate;

# in enviroment install pip dependences
pip install -r requirements.txt --upgrade;

# test application
python main.py;

# exit enviroment
deactivate;
```

## Run swagger local

# Run swagger port 8081
docker run --rm --name swagger-ui -p 8081:8080 -e SWAGGER_JSON=/foo/openapi.yaml -v "$(pwd)":/foo swaggerapi/swagger-ui



## Run

```bash
# Enviroment var is not working currently
export GOOGLE_APPLICATION_CREDENTIALS="america-cloud-befe66bd99af.json";
export GOOGLE_CLOUD_PROJECT="america-cloud";

dev_appserver.py ./ --host="0.0.0.0" --enable_host_checking="false" --log_level="debug";

```

## Edit Frontend
- install **nodejs 8+** and dependencies
    ```bash
    npm i -g stylus nib pug-cli less less-prefixer watch-less http-server bower;
    ```
- html render from pug
    ```bash
    pug -w -P -o ./static/html/ ./static/html/index.pug;
    ```
- css rednder from stylus
    ```bash
    stylus -u nib -w ./static/css/style.styl;
    ```

## Deploy

```bash
 gcloud app deploy ./ --project="xertica-cloud";
```


##Documentación
https://cloud.google.com/compute/docs/tutorials/python-guide

##CREDENCIAL

107534939767484976867



https://www.googleapis.com/auth/ediscovery,
https://www.googleapis.com/auth/cloud-platform,
https://www.googleapis.com/auth/devstorage.full_control,
https://www.googleapis.com/auth/devstorage.read_write

Portal de ejecución:
https://endpointsportal.xertica-vault-backup.cloud.goog/
