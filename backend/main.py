import config
from flask import Flask, render_template
from app_src.endpoints import endpoints
from app_src.view import view
from app_src.task import task

app = Flask(__name__)

app.register_blueprint(endpoints)
app.register_blueprint(view)
app.register_blueprint(task)



if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True)

